# AstroScript #

AstroScript is a scripting language designed to address certain issues encountered by those who have
only just begun to learn how to program. The language was designed and the interpreter created as the
dissertation project for my undergraduate dissertation.

The language/interpreter was designed and implemented entirely by hand using C#, without the use of any
compiler-compilers or similar tools.

Features:
* Object-oriented
* Dynamically typed
* Top level code execution
* Integrated event system (binds to key presses)

The steps a script goes through include:
1. Source is written as text
2. Source is tokenized into a stream of tokens
3. Token stream is parsed (by recursive descent) into a node tree
3. Root node of the tree is executed to execute the entire program

The visual studio solution contains a WPF application which acts as a text editor and execution 
environment, a console app for testing, a project which contains the actual engine code, and finally
a unit testing project.

Team Foundation Server was originally used as the source control method, but now the whole project
has been moved over to git (this explains why the whole project was committed in the initial commit).