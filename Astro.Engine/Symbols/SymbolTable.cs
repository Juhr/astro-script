﻿using Astro.Engine.Parsing;
using Astro.Engine.Symbols.TypeSymbols;
using Astro.Engine.Tokenization;

namespace Astro.Engine.Symbols
{
    /// <summary>
    /// A store of all the symbols created during compilation.
    /// </summary>
    public class SymbolTable
    {
        private Scope mDefaultScope;
        private Scope mCurrentScope;

        /// <summary>
        /// Create the symbol table.
        /// </summary>
        /// <param name="parseNodeFactory">The parse node factory for this program.</param>
        public SymbolTable(ParseNodeFactory parseNodeFactory)
        {
            mDefaultScope = new Scope(ScopeType.Global, null);
            mCurrentScope = mDefaultScope;

            // Add pre-existing keywords and reserved keywords
            this.AddSymbol("var", SymbolType.Declaration);
            this.AddSymbol("if", SymbolType.If);
            this.AddSymbol("else", SymbolType.Else);
            this.AddSymbol("while", SymbolType.While);
            this.AddSymbol("for", SymbolType.For);
            this.AddSymbol("foreach", SymbolType.Foreach);
            this.AddSymbol("in", SymbolType.In);
            this.AddSymbol("define", SymbolType.Define);
            this.AddSymbol("function", SymbolType.Function);
            this.AddSymbol("true", SymbolType.True);
            this.AddSymbol("false", SymbolType.False);
            this.AddSymbol("new", SymbolType.New);
            this.AddSymbol("return", SymbolType.Return);
            this.AddSymbol("constructor", SymbolType.Constructor);
            this.AddSymbol("break", SymbolType.Break);
            this.AddSymbol("static", SymbolType.Static);
            this.AddSymbol("null", SymbolType.Null);

            // Push the scope for global code
            this.PushScope(ScopeType.Global);

            // Add system objects
            this.AddObjectSymbol(new IntegerSymbol(mCurrentScope));
            this.AddObjectSymbol(new FloatSymbol(mCurrentScope));
            this.AddObjectSymbol(new StringSymbol(mCurrentScope));
            this.AddObjectSymbol(new BooleanSymbol(mCurrentScope));
            this.AddObjectSymbol(new ListSymbol(this, parseNodeFactory));
            this.AddObjectSymbol(new DictionarySymbol(this, parseNodeFactory));
            this.AddObjectSymbol(new ConsoleSymbol(this, parseNodeFactory));
            this.AddObjectSymbol(new CommandsSymbol(this, parseNodeFactory));
        }

        /// <summary>
        /// The current scope.
        /// </summary>
        public Scope CurrentScope
        {
            get { return mCurrentScope; }
        }

        /// <summary>
        /// Searches for the symbol with the given identifier starting at the current
        /// scope and visiting each ancestor in turn.
        /// </summary>
        /// <param name="token">The token whose lexeme will be used as an identifier.</param>
        /// <param name="symbol">The output for the symbol.</param>
        /// <returns>Whether the symbol is found.</returns>
        public bool TryGetSymbol(Token token, out Symbol symbol)
        {
            return this.TryGetSymbol(token.Lexeme, out symbol);
        }

        /// <summary>
        /// Searches for the symbol with the given identifier starting at the current
        /// scope and visiting each ancestor in turn.
        /// </summary>
        /// <param name="identifier">The identifier of the symbol to find.</param>
        /// <param name="symbol">The output for the symbol.</param>
        /// <returns>Whether the symbol is found.</returns>
        public bool TryGetSymbol(string identifier, out Symbol symbol)
        {
            var scope = mCurrentScope;
            symbol = null;
            while (scope != null)
            {
                if (scope.Symbols.TryGetValue(identifier, out symbol))
                {
                    return true;
                }
                scope = scope.Parent;
            }
            return false;
        }

        /// <summary>
        /// Searches for the keyword symbol with the given identifier.
        /// </summary>
        /// <param name="identifier">The identifier of the keyword to find.</param>
        /// <param name="symbol">The output for the symbol.</param>
        /// <returns>Whether the symbol is found.</returns>
        public bool TryGetKeywordSymbol(string identifier, out Symbol symbol)
        {
            return mDefaultScope.Symbols.TryGetValue(identifier, out symbol);
        }

        /// <summary>
        /// Push a new scope onto the scope stack.
        /// </summary>
        /// <param name="scopeType">The type of the scope to push.</param>
        /// <returns>The scope that was pushed.</returns>
        public Scope PushScope(ScopeType scopeType)
        {
            mCurrentScope = new Scope(scopeType, mCurrentScope);
            return mCurrentScope;
        }

        /// <summary>
        /// Pop the current scope from the scope stack.
        /// </summary>
        public void PopScope()
        {
            mCurrentScope = mCurrentScope.Parent;
        }

        /// <summary>
        /// Create a new variable symbol for the given token to the current scope.
        /// </summary>
        /// <param name="token">The token for this symbol.</param>
        /// <returns>The newly created symbol.</returns>
        public Symbol CreateVariableSymbol(Token token)
        {
            var symbol = this.AddSymbol(token.Lexeme, SymbolType.VariableName);
            return symbol;
        }

        /// <summary>
        /// Create and add the given function symbol to the current scope.
        /// </summary>
        /// <param name="token">The token for the symbol.</param>
        /// <returns>The newly created function symbol.</returns>
        public FunctionSymbol CreateFunctionSymbol(Token token)
        {
            var identifier = token.Lexeme;

            // Create the symbol
            var symbol = new FunctionSymbol(token, mCurrentScope);

            // Store it
            mCurrentScope.Symbols.Add(identifier, symbol);

            return symbol;
        }

        /// <summary>
        /// Create and add the given object symbol to the current scope.
        /// </summary>
        /// <param name="token">The token for the symbol.</param>
        /// <returns>The newly created object symbol.</returns>
        public ObjectSymbol CreateObjectSymbol(Token token)
        {
            var identifier = token.Lexeme;

            // Create the symbol
            var symbol = new ObjectSymbol(token, mCurrentScope);

            // Store it
            mCurrentScope.Symbols.Add(identifier, symbol);

            return symbol;
        }

        /// <summary>
        /// Add a new symbol to the current scope.
        /// </summary>
        /// <param name="identifier">The identifier for the symbol.</param>
        /// <param name="type">The symbol's type.</param>
        private Symbol AddSymbol(string identifier, SymbolType type)
        {
            var symbol = new Symbol(identifier, type, mCurrentScope);
            mCurrentScope.Symbols.Add(identifier, symbol);
            return symbol;
        }

        /// <summary>
        /// Adds the given object symbol to the current scope.
        /// </summary>
        /// <param name="symbol">The symbol to add.</param>
        private void AddObjectSymbol(ObjectSymbol symbol)
        {
            mCurrentScope.Symbols.Add(symbol.Identifier, symbol);
        }
    }
}
