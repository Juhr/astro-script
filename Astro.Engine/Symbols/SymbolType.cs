﻿
namespace Astro.Engine.Symbols
{
    /// <summary>
    /// Defines the types of symbols.
    /// </summary>
    public enum SymbolType
    {
        /// <summary>
        /// An identifier for a variable.
        /// </summary>
        VariableName = 0,

        /// <summary>
        /// A declaration keyword.
        /// </summary>
        Declaration = 1,

        /// <summary>
        /// An if keyword.
        /// </summary>
        If = 2,

        /// <summary>
        /// An else keyword.
        /// </summary>
        Else = 3,

        /// <summary>
        /// A while keyword.
        /// </summary>
        While = 4,

        /// <summary>
        /// A for keyword.
        /// </summary>
        For = 5,

        /// <summary>
        /// A foreach keyword.
        /// </summary>
        Foreach = 6,

        /// <summary>
        /// An in keyword.
        /// </summary>
        In = 7,

        /// <summary>
        /// A define keyword.
        /// </summary>
        Define = 8,

        /// <summary>
        /// A function keyword.
        /// </summary>
        Function = 9,

        /// <summary>
        /// A boolean true keyword.
        /// </summary>
        True = 10,

        /// <summary>
        /// A boolean false keyword.
        /// </summary>
        False = 11,

        /// <summary>
        /// A new keyword, for object creation.
        /// </summary>
        New = 12,

        /// <summary>
        /// A return keyword.
        /// </summary>
        Return = 13,

        /// <summary>
        /// A constructor keyword.
        /// </summary>
        Constructor = 14,

        /// <summary>
        /// A break keyword.
        /// </summary>
        Break = 15,

        /// <summary>
        /// A static keyword.
        /// </summary>
        Static = 16,

        /// <summary>
        /// A null keyword.
        /// </summary>
        Null = 17,

        /// <summary>
        /// A reserved keyword.
        /// </summary>
        Reserved,

        /// <summary>
        /// A function name.
        /// </summary>
        FunctionName,

        /// <summary>
        /// An object name.
        /// </summary>
        ObjectName,
    }
}
