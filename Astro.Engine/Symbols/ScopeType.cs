﻿
namespace Astro.Engine.Symbols
{
    /// <summary>
    /// Defines the different types of scope.
    /// </summary>
    public enum ScopeType
    {
        /// <summary>
        /// The global scope.
        /// </summary>
        Global,

        /// <summary>
        /// Scope for a function definition.
        /// </summary>
        Function,

        /// <summary>
        /// Scope for an object definition.
        /// </summary>
        Object,

        /// <summary>
        /// Scope for a control structure.
        /// </summary>
        Control,

        /// <summary>
        /// Scope for a loop.
        /// </summary>
        Loop,
    }
}
