﻿using System;
using Astro.Engine.Parsing;
using Astro.Engine.Tokenization;

namespace Astro.Engine.Symbols
{
    /// <summary>
    /// Stores a function's information.
    /// </summary>
    public class FunctionSymbol : Symbol
    {
        private bool mIsInitialised = false;
        private string[] mParameters;
        private ParseNode mParseTree;

        /// <summary>
        /// Create a function symbol.
        /// </summary>
        /// <param name="token">The token for this symbol.</param>
        /// <param name="scope">This symbol's scope.</param>
        public FunctionSymbol(Token token, Scope scope)
            :base(token, SymbolType.FunctionName, scope)
        {
            mIsInitialised = false;
        }

        #region Properties

        /// <summary>
        /// Whether this function symbol has had its parse tree set.
        /// </summary>
        public bool IsInitialised
        {
            get { return mIsInitialised; }
        }

        /// <summary>
        /// The parse tree for this function.
        /// </summary>
        public ParseNode ParseTree
        {
            get { return mParseTree; }
        }

        /// <summary>
        /// The parameters this function takes.
        /// </summary>
        public string[] Parameters
        {
            get { return mParameters; }
        }

        #endregion

        /// <summary>
        /// Initialise the function with its parse tree.
        /// </summary>
        /// <param name="parseTree">The parse tree for this function.</param>
        /// <param name="parameters">The parameters for this function.</param>
        public void Initialise(ParseNode parseTree, string[] parameters)
        {
            // Ensure we haven't already initialised this function
            if (mIsInitialised)
            {
                throw new Exception("Function definition has already been initialised.");
            }
            mIsInitialised = true;

            mParseTree = parseTree;
            mParameters = parameters;
        }
    }
}
