﻿
using Astro.Engine.Tokenization;
namespace Astro.Engine.Symbols
{
    /// <summary>
    /// A symbol defining an identifier and its semantics.
    /// </summary>
    public class Symbol
    {
        /// <summary>
        /// This symbol's identifier.
        /// </summary>
        protected string mIdentifier;

        /// <summary>
        /// This symbol's type.
        /// </summary>
        protected SymbolType mType;

        /// <summary>
        /// The scope of this symbol.
        /// </summary>
        protected Scope mScope;

        /// <summary>
        /// The token responsible for this symbol.
        /// </summary>
        protected Token mToken;

        /// <summary>
        /// Create a new symbol.
        /// </summary>
        /// <param name="identifier">The identifier for this symbol.</param>
        /// <param name="type">This symbol's type.</param>
        /// <param name="scope">This symbol's scope.</param>
        public Symbol(string identifier, SymbolType type, Scope scope)
        {
            mIdentifier = identifier;
            mType = type;
            mScope = scope;
        }

        /// <summary>
        /// Create a new symbol.
        /// </summary>
        /// <param name="token">The token for this symbol.</param>
        /// <param name="type">This symbol's type.</param>
        /// <param name="scope">This symbol's scope.</param>
        public Symbol(Token token, SymbolType type, Scope scope)
        {
            mToken = token;
            mIdentifier = token.Lexeme;
            mType = type;
            mScope = scope;
        }

        #region Properties

        /// <summary>
        /// The identifier for this symbol.
        /// </summary>
        public string Identifier
        {
            get { return mIdentifier; }
        }

        /// <summary>
        /// This symbol's type.
        /// </summary>
        public SymbolType Type
        {
            get { return mType; }
        }

        /// <summary>
        /// The scope this symbol belongs to.
        /// </summary>
        public Scope Scope
        {
            get { return mScope; }
        }

        /// <summary>
        /// The token responsible for this symbol.
        /// </summary>
        public Token Token
        {
            get { return mToken; }
        }

        #endregion
    }
}
