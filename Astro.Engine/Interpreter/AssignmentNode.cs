﻿using Astro.Engine.DataTypes;
using Astro.Engine.Tokenization;

namespace Astro.Engine.Interpreter
{
    /// <summary>
    /// A node for assigning a value to a variable.
    /// </summary>
    public class AssignmentNode : InterpreterParseNode
    {
        private DataSlotNode mAccessorNode;
        private InterpreterParseNode mExpressionNode;

        /// <summary>
        /// Create the assignment node.
        /// </summary>
        /// <param name="token">The token responsible for this node.</param>
        /// <param name="accessorNode">The accessor node for the variable we are assigning to.</param>
        /// <param name="expressionNode">The expression node we are assigning to the variable.</param>
        public AssignmentNode(Token token, InterpreterParseNode accessorNode, InterpreterParseNode expressionNode)
            :base(token)
        {
            mAccessorNode = (DataSlotNode)accessorNode;
            mExpressionNode = expressionNode;
        }

        /// <summary>
        /// Execute this node.
        /// </summary>
        public override Data Execute()
        {
            // Execute the variable
            this.ExecuteNode(mAccessorNode);

            // Execute the expression
            var data = this.ExecuteNode(mExpressionNode);

            // Store the expression data in the variable
            mAccessorNode.DataSlot.Data = data;

            return NullData.Instance;
        }
    }
}
