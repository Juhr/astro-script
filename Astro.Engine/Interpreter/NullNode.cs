﻿using Astro.Engine.DataTypes;
using Astro.Engine.Tokenization;

namespace Astro.Engine.Interpreter
{
    /// <summary>
    /// A node whose value is null.
    /// </summary>
    public class NullNode : InterpreterParseNode
    {
        /// <summary>
        /// Create the null node.
        /// </summary>
        /// <param name="token">The token responsible for this node.</param>
        public NullNode(Token token)
            :base(token)
        { }

        /// <summary>
        /// Execute the node.
        /// </summary>
        /// <returns>The data at this node.</returns>
        public override Data Execute()
        {
            return NullData.Instance;
        }
    }
}
