﻿using Astro.Engine.DataTypes;
using Astro.Engine.Tokenization;

namespace Astro.Engine.Interpreter
{
    /// <summary>
    /// A node for a logical OR.
    /// </summary>
    public class LogicalOrNode : InterpreterParseNode
    {
        private InterpreterParseNode mLeftNode;
        private InterpreterParseNode mRightNode;

        /// <summary>
        /// Create the logical OR node.
        /// </summary>
        /// <param name="token">The token responsible for this node.</param>
        /// <param name="leftNode">The left node for the OR.</param>
        /// <param name="rightNode">The right node for the OR.</param>
        public LogicalOrNode(Token token, InterpreterParseNode leftNode, InterpreterParseNode rightNode)
            :base(token)
        {
            mLeftNode = leftNode;
            mRightNode = rightNode;
        }

        /// <summary>
        /// Execute this logical OR.
        /// </summary>
        /// <returns>The result of the OR.</returns>
        public override Data Execute()
        {
            if (this.ExecuteNodeAsBoolean(mLeftNode).Value | this.ExecuteNodeAsBoolean(mRightNode).Value)
            {
                return BooleanData.TrueInstance;
            }
            else
            {
                return BooleanData.FalseInstance;
            }
        }
    }
}
