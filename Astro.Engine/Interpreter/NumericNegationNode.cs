﻿using Astro.Engine.DataTypes;
using Astro.Engine.ErrorReporting;
using Astro.Engine.Tokenization;

namespace Astro.Engine.Interpreter
{
    /// <summary>
    /// A node for negating numeric nodes.
    /// </summary>
    public class NumericNegationNode : InterpreterParseNode
    {
        private InterpreterParseNode mToNegate;

        /// <summary>
        /// Create a numeric negation node.
        /// </summary>
        /// <param name="token">The token responsible for this node.</param>
        /// <param name="toNegate">The node to negate.</param>
        public NumericNegationNode(Token token, InterpreterParseNode toNegate)
            :base(token)
        {
            mToNegate = toNegate;
        }

        /// <summary>
        /// Execute the node.
        /// </summary>
        /// <returns>The negated data.</returns>
        public override Data Execute()
        {
            // Execute the node to negate
            var data = mToNegate.Execute();

            // Check the data type
            if (data.Type == DataType.Integer)
            {
                return new IntegerData(-((IntegerData)data).Value);
            }
            else if (data.Type == DataType.Float)
            {
                return new FloatData(-((FloatData)data).Value);
            }
            else
            {
                throw new RuntimeException(mToNegate.Token, ErrorCode.NonNumericDataType, "Cannot perform a numeric negation on a non-numeric data type.");
            }
        }
    }
}
