﻿using System;
using Astro.Engine.DataTypes;
using Astro.Engine.ErrorReporting;
using Astro.Engine.Tokenization;

namespace Astro.Engine.Interpreter
{
    /// <summary>
    /// Parse node for the list object's Contains function.
    /// </summary>
    public class ListContainsNode : InterpreterParseNode
    {
        /// <summary>
        /// Create a new list remove at node.
        /// </summary>
        public ListContainsNode()
            : base(new Token())
        { }

        /// <summary>
        /// Execute the node.
        /// </summary>
        public override Data Execute()
        {
            // Get the data
            var data = mExecutionEnvironment.GetDataSlot("*item").Data;

            // Get the list
            var list = ((EngineListData)mExecutionEnvironment.GetDataSlot("*list").Data).Value;

            // Check whether the list contains the data
            if (list.Contains(data))
            {
                return BooleanData.TrueInstance;
            }
            else
            {
                return BooleanData.FalseInstance;
            }
        }
    }
}
