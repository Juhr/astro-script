﻿using Astro.Engine.DataTypes;
using Astro.Engine.Tokenization;

namespace Astro.Engine.Interpreter
{
    /// <summary>
    /// An if condition node.
    /// </summary>
    public class IfControlNode : InterpreterParseNode
    {
        private InterpreterParseNode mConditionNode;
        private InterpreterParseNode mBlockNode;

        /// <summary>
        /// Create the if control node.
        /// </summary>
        /// <param name="token">The token responsible for this node.</param>
        /// <param name="conditionNode">The condition node.</param>
        /// <param name="blockNode">The block node.</param>
        public IfControlNode(Token token, InterpreterParseNode conditionNode, InterpreterParseNode blockNode)
            :base(token)
        {
            mConditionNode = conditionNode;
            mBlockNode = blockNode;
        }

        /// <summary>
        /// Execute this ndoe.
        /// </summary>
        public override Data Execute()
        {
            // Push a stack frame
            mExecutionEnvironment.PushStackFrame();

            BooleanData data;
            try
            {
                // Execute the condition
                data = this.ExecuteNodeAsBoolean(mConditionNode);

                // Execute the block if the condition was true
                if (data.Value)
                {
                    this.ExecuteNode(mBlockNode);
                }
            }
            finally
            {
                mExecutionEnvironment.PopStackFrame();
            }

            return data;
        }
    }
}
