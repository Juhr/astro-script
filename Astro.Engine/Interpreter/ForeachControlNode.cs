﻿using System;
using Astro.Engine.DataTypes;
using Astro.Engine.ErrorReporting;
using Astro.Engine.Tokenization;

namespace Astro.Engine.Interpreter
{
    /// <summary>
    /// A foreach control node.
    /// </summary>
    public class ForeachControlNode : LoopNode
    {
        private DeclarationNode mDeclarationNode;
        private InterpreterParseNode mVariabelNode;
        private InterpreterParseNode mBlockNode;

        /// <summary>
        /// Create a foreach node.
        /// </summary>
        /// <param name="token">The token responsible for this node.</param>
        /// <param name="declarationNode">The variable declaration for this control.</param>
        /// <param name="variableNode">The variable we are iterating over.</param>
        /// <param name="blockNode">The block we are looping.</param>
        public ForeachControlNode(Token token, InterpreterParseNode declarationNode, InterpreterParseNode variableNode, InterpreterParseNode blockNode)
            :base(token)
        {
            mDeclarationNode = (DeclarationNode)declarationNode;
            mVariabelNode = variableNode;
            mBlockNode = blockNode;
        }

        /// <summary>
        /// Execute this node.
        /// </summary>
        public override Data Execute()
        {
            mIsLoopBroken = false;

            // Push the stack frame
            mExecutionEnvironment.PushStackFrame();

            try
            {
                // Declare the variable and get it's dataslot
                this.ExecuteNode(mDeclarationNode);
                var variableSlot = mExecutionEnvironment.GetDataSlot(mDeclarationNode.Symbol.Identifier);

                // Get hold of the variable we will iterate over
                var data = this.ExecuteNode(mVariabelNode);

                // Find whether we're iterating over a list or a dictionary
                var listData = data as ListData;
                if (listData != null)
                {
                    // Push the frame for the block
                    mExecutionEnvironment.PushStackFrame();

                    try
                    {
                        // Get the list and iterate over it
                        var list = ((EngineListData)listData.Variables["*list"].Data).Value;
                        foreach (var item in list)
                        {
                            // Assign the data
                            variableSlot.Data = item;

                            // Execute the block
                            this.ExecuteNode(mBlockNode);

                            // Clear the stack frame
                            mExecutionEnvironment.ClearStackFrame();
                        }
                    }
                    finally
                    {
                        mExecutionEnvironment.PopStackFrame();
                    }
                }
                else
                {
                    var dictionaryData = data as DictionaryData;
                    if (dictionaryData != null)
                    {

                    }
                    else
                    {
                        throw new RuntimeException(mToken, ErrorCode.CollectionObjectExpected, "Only objects of type 'List' or 'Dictionary' can be used with a 'foreach' loop.");
                    }
                }
            }
            finally
            {
                mExecutionEnvironment.PopStackFrame();
            }
            
            return null;
        }
    }
}
