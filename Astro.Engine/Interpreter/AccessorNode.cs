﻿using System;
using Astro.Engine.DataTypes;
using Astro.Engine.ErrorReporting;
using Astro.Engine.Symbols;
using Astro.Engine.Tokenization;

namespace Astro.Engine.Interpreter
{
    /// <summary>
    /// Node for accessing members of other variables/objects.
    /// </summary>
    public class AccessorNode : DataSlotNode
    {
        private string mMemberName;
        private InterpreterParseNode mToAccess;

        /// <summary>
        /// Create the accessor node.
        /// </summary>
        /// <param name="token">The token responsible for this node.</param>
        /// <param name="toAccess">The node we are accessing.</param>
        public AccessorNode(Token token, InterpreterParseNode toAccess)
            :base(token)
        {
            mMemberName = token.Lexeme;
            mToAccess = toAccess;
        }

        /// <summary>
        /// Execute this node.
        /// </summary>
        public override Data Execute()
        {
            // Execute the node
            var data = this.ExecuteNode(mToAccess);

            try
            {
                DataSlot dataSlot;
                FunctionSymbol functionSymbol;

                // Check for a static object (This code is slow but can/should be improved in the future)
                var staticObject = mToAccess as ObjectNode;
                if (staticObject != null)
                {
                    var objectSymbol = (ObjectSymbol)staticObject.Symbol;

                    // Check for a static variable
                    if (objectSymbol.StaticVariables.TryGetValue(mMemberName, out dataSlot))
                    {
                        mDataSlot = dataSlot;
                        return dataSlot.Data;
                    }

                    // Check for a static function
                    if (objectSymbol.StaticFunctions.TryGetValue(mMemberName, out functionSymbol))
                    {
                        return new FunctionData(functionSymbol, objectSymbol.StaticVariables);
                    }
                    else
                    {
                        // Throw exception since we can't find the member
                        throw new RuntimeException(mToAccess.Token, ErrorCode.DoesNotContainMember,
                                                         string.Format("Object definition '{0}' does not contain a member by the name '{1}'.", objectSymbol.Identifier, mMemberName));
                    }
                }

                // Cast the data and check for a member variable
                var objectData = (ObjectData)data;
                if (objectData.Variables.TryGetValue(mMemberName, out dataSlot))
                {
                    mDataSlot = dataSlot;
                    return dataSlot.Data;
                }

                // Check for a member function or static function instead
                if (objectData.Symbol.MemberFunctions.TryGetValue(mMemberName, out functionSymbol) ||
                    objectData.Symbol.StaticFunctions.TryGetValue(mMemberName, out functionSymbol))
                {
                    // Set the object context for the object we are accessing
                    return new FunctionData(functionSymbol, objectData.Variables);
                }
                else
                {
                    // Throw exception since we can't find the member
                    throw new RuntimeException(mToAccess.Token, ErrorCode.DoesNotContainMember,
                                                     string.Format("Object definition '{0}' does not contain a member by the name '{1}'.", objectData.Symbol.Identifier, mMemberName));
                }
            }
            catch (InvalidCastException e)
            {
                throw new NotImplementedException(e.Message);
            }
        }
    }
}
