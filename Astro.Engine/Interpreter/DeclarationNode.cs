﻿using Astro.Engine.DataTypes;
using Astro.Engine.Symbols;
using Astro.Engine.Tokenization;

namespace Astro.Engine.Interpreter
{
    /// <summary>
    /// A declaration node.
    /// </summary>
    public class DeclarationNode : VariableNode
    {
        /// <summary>
        /// Create the declaration node.
        /// </summary>
        /// <param name="token">The token responsible for this node.</param>
        /// <param name="variableSymbol">The variable we are declaring.</param>
        public DeclarationNode(Token token, Symbol variableSymbol)
            :base(token, variableSymbol)
        { }

        /// <summary>
        /// Execute this declaration.
        /// </summary>
        public override Data Execute()
        {
            // Create memory for this variable on the stack
            mDataSlot = mExecutionEnvironment.CreateDataSlot(mVariableSymbol.Identifier);

            return NullData.Instance;
        }
    }
}
