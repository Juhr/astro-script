﻿using System;
using Astro.Engine.DataTypes;
using Astro.Engine.ErrorReporting;
using Astro.Engine.Tokenization;

namespace Astro.Engine.Interpreter
{
    /// <summary>
    /// Parse node for the dictionary object's Count function.
    /// </summary>
    public class DictionaryCountNode : InterpreterParseNode
    {
        /// <summary>
        /// Create a new dictionary count at node.
        /// </summary>
        public DictionaryCountNode()
            : base(new Token())
        { }

        /// <summary>
        /// Execute the node.
        /// </summary>
        public override Data Execute()
        {
            // Get the dictionary and return its count
            this.FindCallingFunctionNode().ReturnData = new IntegerData(((EngineDictionaryData)mExecutionEnvironment.GetDataSlot("*dictionary").Data).Value.Count);
            return null;
        }
    }
}
