﻿using Astro.Engine.DataTypes;
using Astro.Engine.Tokenization;

namespace Astro.Engine.Interpreter
{
    /// <summary>
    /// Node for returning values from functions.
    /// </summary>
    public class ReturnValueNode : ReturnNode
    {
        private InterpreterParseNode mReturnExpression;

        /// <summary>
        /// Create a return value node.
        /// </summary>
        /// <param name="token">The token responsible for this node.</param>
        /// <param name="returnExpression">The node for the return expression.</param>
        public ReturnValueNode(Token token, InterpreterParseNode returnExpression)
            :base(token)
        {
            mReturnExpression = returnExpression;
        }

        /// <summary>
        /// Execute this node.
        /// </summary>
        /// <returns></returns>
        public override Data Execute()
        {
            // Set the return value
            mReturnData = this.ExecuteNode(mReturnExpression);

            // Return from calling nodes
            base.Execute();

            return null;
        }
    }
}
