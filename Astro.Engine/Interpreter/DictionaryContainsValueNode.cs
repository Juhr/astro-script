﻿using System;
using Astro.Engine.DataTypes;
using Astro.Engine.ErrorReporting;
using Astro.Engine.Tokenization;

namespace Astro.Engine.Interpreter
{
    /// <summary>
    /// Parse node for the dictionary object's ContainsValue function.
    /// </summary>
    public class DictionaryContainsValueNode : InterpreterParseNode
    {
        /// <summary>
        /// Create a new dictionary contains value node.
        /// </summary>
        public DictionaryContainsValueNode()
            : base(new Token())
        { }

        /// <summary>
        /// Execute the node.
        /// </summary>
        public override Data Execute()
        {
            // Get the data
            var value = mExecutionEnvironment.GetDataSlot("*value").Data;

            // Get the dictionary
            var dictionary = ((EngineDictionaryData)mExecutionEnvironment.GetDataSlot("*dictionary").Data).Value;

            // Check whether the dictionary contains the value
            if (dictionary.ContainsValue(value))
            {
                this.FindCallingFunctionNode().ReturnData = BooleanData.TrueInstance;
            }
            else
            {
                this.FindCallingFunctionNode().ReturnData = BooleanData.FalseInstance;
            }
            return null;
        }
    }
}
