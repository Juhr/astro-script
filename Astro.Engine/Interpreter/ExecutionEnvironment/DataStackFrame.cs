﻿using System.Collections.Generic;
using Astro.Engine.DataTypes;

namespace Astro.Engine.Interpreter.ExecutionEnvironment
{
    /// <summary>
    /// A frame in the data stack. Contains declared variables.
    /// </summary>
    public class DataStackFrame
    {
        private DataStackFrame mParent;
        private Dictionary<string, DataSlot> mDataSlots = new Dictionary<string, DataSlot>();
        private List<DataSlot> mFreeableDataSlots = new List<DataSlot>();

        /// <summary>
        /// Create the stack frame.
        /// </summary>
        /// <param name="parent">The parent frame for this frame.</param>
        public DataStackFrame(DataStackFrame parent)
        {
            mParent = parent;
        }

        #region Properties

        /// <summary>
        /// This frame's parent.
        /// </summary>
        public DataStackFrame Parent
        {
            get { return mParent; }
        }

        /// <summary>
        /// This frame's data slots.
        /// </summary>
        public Dictionary<string, DataSlot> DataSlots
        {
            get { return mDataSlots; }
        }

        /// <summary>
        /// This frame's freeable data slots.
        /// </summary>
        public List<DataSlot> FreeableDataSlots
        {
            get { return mFreeableDataSlots; }
        }

        #endregion

        /// <summary>
        /// Adds a data slot to this stack frame.
        /// </summary>
        /// <param name="identifier">The identifier to use.</param>
        /// <param name="dataSlot">The data slot to use.</param>
        public void AddDataSlot(string identifier, DataSlot dataSlot)
        {
            mDataSlots.Add(identifier, dataSlot);
        }

        /// <summary>
        /// Add a data slot which will later be freeable once this stack is used.
        /// </summary>
        /// <param name="identifier">The identifier to use.</param>
        /// <param name="dataSlot">The data slot to use.</param>
        public void AddFreeableDataSlot(string identifier, DataSlot dataSlot)
        {
            mDataSlots.Add(identifier, dataSlot);
            mFreeableDataSlots.Add(dataSlot);
        }

        /// <summary>
        /// Set the data at the slot for the given identifier.
        /// </summary>
        /// <param name="identifier">The identifier for the data slot.</param>
        /// <param name="data">The data to store.</param>
        public void SetData(string identifier, Data data)
        {
            mDataSlots[identifier].Data = data;
        }

        /// <summary>
        /// Retrieves the data slot with the given identifier. Looks up the frame stack
        /// if it is not present in the current frame.
        /// </summary>
        /// <param name="identifier">The identifier of the data slot to retrieve.</param>
        /// <returns>The data slot with the given identifier.</returns>
        public DataSlot GetDataSlot(string identifier)
        {
            DataSlot dataSlot;
            if (!mDataSlots.TryGetValue(identifier, out dataSlot))
            {
                return mParent.GetDataSlot(identifier);
            }

            return dataSlot;
        }
    }
}
