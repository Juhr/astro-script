﻿using System;
using System.Collections.Generic;
using Astro.Engine.DataTypes;
using Astro.Engine.ErrorReporting;

namespace Astro.Engine.Interpreter.ExecutionEnvironment
{
    /// <summary>
    /// The execution environment for an interpreted program.
    /// </summary>
    public class InterpreterExecutionEnvironment : IDisposable
    {
        private CommandsManager mCommandsManager;
        private ErrorLog mErrorLog;
        private DataStackFrame mStackFrame = new DataStackFrame(null);
        private Stack<DataSlot> mDataSlotPool = new Stack<DataSlot>();
        private IConsoleOutput mConsoleOutput;

        /// <summary>
        /// Create the execution environment.
        /// </summary>
        /// <param name="errorLog">The error log.</param>
        /// <param name="eventListener">The event listener for this execution environment's commands manager.</param>
        /// <param name="consoleOutput">The console output for this execution environment.</param>
        public InterpreterExecutionEnvironment(ErrorLog errorLog, ICommandsEventListener eventListener, IConsoleOutput consoleOutput)
        {
            mConsoleOutput = consoleOutput;
            mCommandsManager = new CommandsManager(this, eventListener);
            mErrorLog = errorLog;
            
            // Add data slots to the pool
            for (int i = 0; i < 500; i++)
            {
                mDataSlotPool.Push(new DataSlot());
            }
        }

        #region Properties

        /// <summary>
        /// The error log for this environment.
        /// </summary>
        public ErrorLog ErrorLog
        {
            get { return mErrorLog; }
        }

        /// <summary>
        /// The commands manager for this environment.
        /// </summary>
        public CommandsManager CommandsManager
        {
            get { return mCommandsManager; }
        }

        /// <summary>
        /// The console output for this environment.
        /// </summary>
        public IConsoleOutput Console
        {
            get { return mConsoleOutput; }
        }

        #endregion

        /// <summary>
        /// Adds a data slot from the pool to the current stack frame and assigns it the given identifier.
        /// </summary>
        /// <param name="identifier">The identifier for the data slot to add.</param>
        public DataSlot CreateDataSlot(string identifier)
        {
            DataSlot dataSlot;
            try
            {
                dataSlot = mDataSlotPool.Pop();
            }
            catch (InvalidOperationException)
            {
                // Pool has ran out of slots, add 500 new ones
                for (int i = 0; i < 499; i++)
                {
                    mDataSlotPool.Push(new DataSlot());
                }
                dataSlot = new DataSlot();
            }
            mStackFrame.AddFreeableDataSlot(identifier, dataSlot);
            return dataSlot;
        }

        /// <summary>
        /// Adds a data slot from the pool to the current stack frame and assigns it the given identifier and data.
        /// </summary>
        /// <param name="identifier">The identifier for the data slot to add.</param>
        /// <param name="data">The data to assign to the data slot.</param>
        public void CreateDataSlot(string identifier, Data data)
        {
            var dataSlot = this.CreateDataSlot(identifier);
            dataSlot.Data = data;
        }

        /// <summary>
        /// Add an existing data slot to the current stack frame.
        /// </summary>
        /// <param name="identifier">The identifier to use for this data slot.</param>
        /// <param name="dataSlot">The data slot to add.</param>
        public void AddDataSlot(string identifier, DataSlot dataSlot)
        {
            mStackFrame.AddDataSlot(identifier, dataSlot);
        }

        /// <summary>
        /// Retrieves the data slot with the given identifier.
        /// </summary>
        /// <param name="identifier">The identifier of the data slot to retrieve.</param>
        /// <returns>The data slot with the specified identifier.</returns>
        public DataSlot GetDataSlot(string identifier)
        {
            return mStackFrame.GetDataSlot(identifier);
        }

        /// <summary>
        /// Pushes a new frame onto the stack, keeping track of newly created data.
        /// </summary>
        public void PushStackFrame()
        {
            mStackFrame = new DataStackFrame(mStackFrame);
        }

        /// <summary>
        /// Pops the a frame from the stack and removes any identifiers
        /// and data that were created as part of it.
        /// </summary>
        public void PopStackFrame()
        {
            // Free this stack's data slots
            foreach (var dataSlot in mStackFrame.FreeableDataSlots)
            {
                mDataSlotPool.Push(dataSlot);
            }
            mStackFrame = mStackFrame.Parent;
        }

        /// <summary>
        /// Clears the current stack frame of data slots.
        /// </summary>
        public void ClearStackFrame()
        {
            // Free this stack's data slots
            foreach (var dataSlot in mStackFrame.FreeableDataSlots)
            {
                mDataSlotPool.Push(dataSlot);
            }
            mStackFrame.DataSlots.Clear();
        }

        /// <summary>
        /// Executes the given program.
        /// </summary>
        /// <param name="programNode">The parse node for the program to execute.</param>
        /// <returns>Whether the execution was a success.</returns>
        public bool ExecuteProgram(InterpreterParseNode programNode)
        {
            // Execute the program and catch runtime exceptions
            try
            {
                programNode.SetExecutionEnvironment(this);
                programNode.Execute();
                return true;
            }
            catch (RuntimeException e)
            {
                // Log the error
                mErrorLog.LogError(e.ErrorCode, e.Message, e.Token.Line, e.Token.Column);
            }

            return false;
        }

        /// <summary>
        /// Dispose of the execution environment.
        /// </summary>
        public void Dispose()
        {
            mCommandsManager.Dispose();
        }
    }
}