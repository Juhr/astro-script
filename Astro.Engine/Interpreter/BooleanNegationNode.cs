﻿using Astro.Engine.DataTypes;
using Astro.Engine.ErrorReporting;
using Astro.Engine.Tokenization;

namespace Astro.Engine.Interpreter
{
    /// <summary>
    /// A node for negating boolean nodes.
    /// </summary>
    public class BooleanNegationNode : InterpreterParseNode
    {
        private InterpreterParseNode mToNegate;

        /// <summary>
        /// Create a boolean negation node.
        /// </summary>
        /// <param name="token">The token responsible for this node.</param>
        /// <param name="toNegate">The node to negate.</param>
        public BooleanNegationNode(Token token, InterpreterParseNode toNegate)
            :base(token)
        {
            mToNegate = toNegate;
        }

        /// <summary>
        /// Execute the node.
        /// </summary>
        /// <returns>The negated data.</returns>
        public override Data Execute()
        {
            // Execute the node to negate
            var data = this.ExecuteNode(mToNegate);

            // Check the data type
            if (data.Type != DataType.Boolean)
            {
                throw new RuntimeException(mToNegate.Token, ErrorCode.NonBooleanDataType, "Cannot perform a boolean negation on a non-boolean data type.");
            }

            // Negate and return the value
            return ((BooleanData)data).Negate();
        }
    }
}
