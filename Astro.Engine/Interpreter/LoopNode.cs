﻿using Astro.Engine.Tokenization;

namespace Astro.Engine.Interpreter
{
    /// <summary>
    /// A node which will contain a loop.
    /// </summary>
    public abstract class LoopNode : InterpreterParseNode
    {
        /// <summary>
        /// Whether the loop at this node has been broken.
        /// </summary>
        protected bool mIsLoopBroken;

        /// <summary>
        /// Create the loop node.
        /// </summary>
        /// <param name="token">The token responsible for this node.</param>
        public LoopNode(Token token)
            :base(token)
        { }

        /// <summary>
        /// Break the loop at this node.
        /// </summary>
        public void BreakLoop()
        {
            mIsLoopBroken = true;
        }
    }
}
