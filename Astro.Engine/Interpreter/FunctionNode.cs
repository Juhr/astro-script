﻿using Astro.Engine.DataTypes;
using Astro.Engine.Symbols;
using Astro.Engine.Tokenization;

namespace Astro.Engine.Interpreter
{
    /// <summary>
    /// A node for a function.
    /// </summary>
    public class FunctionNode : InterpreterParseNode
    {
        private FunctionSymbol mSymbol;
        private FunctionData mFunctionData;

        /// <summary>
        /// Create the function node.
        /// </summary>
        /// <param name="token">The token responsible for this node.</param>
        /// <param name="symbol">The symbol for this function.</param>
        public FunctionNode(Token token, FunctionSymbol symbol)
            :base(token)
        {
            mSymbol = symbol;
            mFunctionData = new FunctionData(mSymbol);
        }

        /// <summary>
        /// Execute this node.
        /// </summary>
        public override Data Execute()
        {
            return mFunctionData;
        }
    }
}
