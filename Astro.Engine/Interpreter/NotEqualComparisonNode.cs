﻿using Astro.Engine.DataTypes;
using Astro.Engine.Tokenization;

namespace Astro.Engine.Interpreter
{
    /// <summary>
    /// A node for an equality comparison.
    /// </summary>
    public class NotEqualComparisonNode : InterpreterParseNode
    {
        private InterpreterParseNode mLeftNode;
        private InterpreterParseNode mRightNode;

        /// <summary>
        /// Create the equality comparison.
        /// </summary>
        /// <param name="token">The token responsible for this node.</param>
        /// <param name="leftNode">The left side node of the comparison.</param>
        /// <param name="rightNode">The right side node of the comparison.</param>
        public NotEqualComparisonNode(Token token, InterpreterParseNode leftNode, InterpreterParseNode rightNode)
            : base(token)
        {
            mLeftNode = leftNode;
            mRightNode = rightNode;
        }

        /// <summary>
        /// Execute the comparison.
        /// </summary>
        /// <returns>The result of the comparison.</returns>
        public override Data Execute()
        {
            return (this.ExecuteNode(mLeftNode).EqualityComparison(this.ExecuteNode(mRightNode)) ? BooleanData.FalseInstance : BooleanData.TrueInstance);
        }
    }
}
