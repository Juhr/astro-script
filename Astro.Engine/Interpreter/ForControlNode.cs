﻿using Astro.Engine.DataTypes;
using Astro.Engine.Tokenization;

namespace Astro.Engine.Interpreter
{
    /// <summary>
    /// Parse node for a for loop control.
    /// </summary>
    public class ForControlNode : LoopNode
    {
        private InterpreterParseNode mDeclarationNode;
        private InterpreterParseNode mConditionNode;
        private InterpreterParseNode mBlockNode;
        private bool mHasDeclaration;
        private bool mHasCondition;

        /// <summary>
        /// Create a for control node.
        /// </summary>
        /// <param name="token">The token responsible for this node.</param>
        /// <param name="declarationNode">The optional declaration node.</param>
        /// <param name="conditionNode">The optional condition node.</param>
        /// <param name="assignmentNode">The optional assignment node.</param>
        /// <param name="blockNode">The block node.</param>
        public ForControlNode(Token token, InterpreterParseNode declarationNode, InterpreterParseNode conditionNode, InterpreterParseNode assignmentNode, InterpreterParseNode blockNode)
            :base(token)
        {
            mDeclarationNode = declarationNode;
            mConditionNode = conditionNode;
            mBlockNode = blockNode;
            mHasDeclaration = mDeclarationNode != null;
            mHasCondition = mConditionNode != null;
            if (assignmentNode != null)
            {
                ((AggregateNode)mBlockNode).AddNode(assignmentNode);
            }
        }

        /// <summary>
        /// Execute this node.
        /// </summary>
        public override Data Execute()
        {
            mIsLoopBroken = false;
            var loopStackFrame = false;

            try
            {
                // Execute the declaration and push the frame
                if (mHasDeclaration)
                {
                    mExecutionEnvironment.PushStackFrame();
                    loopStackFrame = true;
                    this.ExecuteNode(mDeclarationNode);
                }

                // Push a frame for the loop's block
                mExecutionEnvironment.PushStackFrame();

                try
                {
                    // Loop while the condition is true
                    while (!mIsLoopBroken && (!mHasCondition || this.ExecuteNodeAsBoolean(mConditionNode).Value))
                    {
                        // Execute the block
                        this.ExecuteNode(mBlockNode);

                        // Clear the frame for the block
                        mExecutionEnvironment.ClearStackFrame();
                    }
                }
                finally
                {
                    mExecutionEnvironment.PopStackFrame();
                }
            }
            finally
            {
                // Only pop the loop's frame if it was pushed
                if (loopStackFrame)
                {
                    mExecutionEnvironment.PopStackFrame();
                }
            }
            
            return null;
        }
    }
}
