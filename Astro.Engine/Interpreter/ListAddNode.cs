﻿using Astro.Engine.DataTypes;
using Astro.Engine.Tokenization;

namespace Astro.Engine.Interpreter
{
    /// <summary>
    /// Parse node for the list object's Add function.
    /// </summary>
    public class ListAddNode : InterpreterParseNode
    {
        /// <summary>
        /// Create a new list add node.
        /// </summary>
        public ListAddNode()
            :base(new Token())
        { }

        /// <summary>
        /// Execute the node.
        /// </summary>
        public override Data Execute()
        {
            // Get the toAdd data
            var toAdd = mExecutionEnvironment.GetDataSlot("*toAdd").Data;

            // Get the list we are adding to
            var list = ((EngineListData)mExecutionEnvironment.GetDataSlot("*list").Data).Value;

            // Add the item to the list
            list.Add(toAdd.Copy());

            return NullData.Instance;
        }
    }
}
