﻿using System;
using Astro.Engine.Tokenization;

namespace Astro.Engine.ErrorReporting
{
    /// <summary>
    /// Defines a runtime exception.
    /// </summary>
    public class RuntimeException : Exception
    {
        private ErrorCode mErrorCode;
        private Token mToken;

        /// <summary>
        /// Create a new runtime exception.
        /// </summary>
        /// <param name="token">The token that this error occured at.</param>
        /// <param name="errorCode">The error code describing this runtime exception.</param>
        /// <param name="message">The message for this error.</param>
        public RuntimeException(Token token, ErrorCode errorCode, string message)
            : base(message)
        {
            mToken = token;
            mErrorCode = errorCode;
        }

        /// <summary>
        /// The token that this error occured at.
        /// </summary>
        public Token Token
        {
            get { return mToken; }
            set { mToken = value; }
        }

        /// <summary>
        /// The error code describing this runtime exception.
        /// </summary>
        public ErrorCode ErrorCode
        {
            get { return mErrorCode; }
        }
    }
}
