﻿using System.Collections.Generic;
namespace Astro.Engine.ErrorReporting
{
    /// <summary>
    /// Stores errors reported during compile time and run time.
    /// </summary>
    public class ErrorLog
    {
        private List<LogEntry> mEntries = new List<LogEntry>();

        /// <summary>
        /// Create an error log.
        /// </summary>
        public ErrorLog()
        { }

        #region Properties

        /// <summary>
        /// The entries in this log.
        /// </summary>
        public List<LogEntry> Entries
        {
            get { return mEntries; }
        }

        #endregion

        /// <summary>
        /// Log an error.
        /// </summary>
        /// <param name="code"></param>
        /// <param name="message"></param>
        /// <param name="line">The line the error occured on.</param>
        /// <param name="column">The column the error occured on.</param>
        public void LogError(ErrorCode code, string message, int line, int column)
        {
            mEntries.Add(new LogEntry(code, message, line, column));
        }
    }
}
