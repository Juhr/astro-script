﻿
namespace Astro.Engine.ErrorReporting
{
    /// <summary>
    /// An entry into the error log.
    /// </summary>
    public class LogEntry
    {
        private ErrorCode mErrorCode;
        private string mMessage;
        private int mLine;
        private int mColumn;

        /// <summary>
        /// Creates a new error log entry.
        /// </summary>
        /// <param name="errorCode">The code for this error.</param>
        /// <param name="message">The message for this error.</param>
        /// <param name="line">The line the error occured on.</param>
        /// <param name="column">The column the error occured on.</param>
        public LogEntry(ErrorCode errorCode, string message, int line, int column)
        {
            mErrorCode = errorCode;
            mMessage = message;
            mLine = line;
            mColumn = column;
        }

        /// <summary>
        /// Creates a new error log entry.
        /// </summary>
        public LogEntry()
        { }

        #region Properties

        /// <summary>
        /// The code for this error.
        /// </summary>
        public ErrorCode ErrorCode
        {
            get { return mErrorCode; }
        }

        /// <summary>
        /// The message for this error.
        /// </summary>
        public string Message
        {
            get { return mMessage; }
        }

        /// <summary>
        /// The line in the source that this error occured on.
        /// </summary>
        public int Line
        {
            get { return mLine; }
        }

        /// <summary>
        /// The column in the source that this error occured on.
        /// </summary>
        public int Column
        {
            get { return mColumn; }
        }

        #endregion
    }
}
