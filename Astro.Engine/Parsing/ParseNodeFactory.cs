﻿using System.Collections.Generic;
using Astro.Engine.ErrorReporting;
using Astro.Engine.Symbols;
using Astro.Engine.Tokenization;

namespace Astro.Engine.Parsing
{
    /// <summary>
    /// The basis for a parse node factory.
    /// </summary>
    public abstract class ParseNodeFactory
    {
        /// <summary>
        /// Create a literal value node from a literal type token.
        /// </summary>
        /// <param name="token">The token to create the value from.</param>
        public abstract ParseNode CreateLiteralNode(Token token);

        /// <summary>
        /// Create a boolean negation node.
        /// </summary>
        /// <param name="token">The token for the node.</param>
        /// <param name="node">The node to negate.</param>
        public abstract ParseNode CreateBooleanNegationNode(Token token, ParseNode node);

        /// <summary>
        /// Create a numeric negation node.
        /// </summary>
        /// <param name="token">The token for the node.</param>
        /// <param name="node">The node to negate.</param>
        public abstract ParseNode CreateNumericNegationNode(Token token, ParseNode node);

        /// <summary>
        /// Create an operator node.
        /// </summary>
        /// <param name="token">The token for the node.</param>
        /// <param name="leftNode">The node for the left side of the operation.</param>
        /// <param name="rightNode">The node for the right side of the operation.</param>
        public abstract ParseNode CreateOperatorNode(Token token, ParseNode leftNode, ParseNode rightNode);

        /// <summary>
        /// Create a variable node.
        /// </summary>
        /// <param name="token">The token for the node.</param>
        /// <param name="variableSymbol">The symbol for this variable.</param>
        public abstract ParseNode CreateVariableNode(Token token, Symbol variableSymbol);

        /// <summary>
        /// Create a declaration node.
        /// </summary>
        /// <param name="token">The token responsible for this node.</param>
        /// <param name="variableSymbol">The symbol for this variable.</param>
        public abstract ParseNode CreateDeclarationNode(Token token, Symbol variableSymbol);

        /// <summary>
        /// Create an assignment node.
        /// </summary>
        /// <param name="token">The token responsible for this node.</param>
        /// <param name="variableNode">The variable node for the assignment.</param>
        /// <param name="expressionNode">The expression node for the assignment.</param>
        public abstract ParseNode CreateAssignmentNode(Token token, ParseNode variableNode, ParseNode expressionNode);

        /// <summary>
        /// Create an accessor node.
        /// </summary>
        /// <param name="token">THe token responsible for this node.</param>
        /// <param name="node">The node we are accessing.</param>
        public abstract ParseNode CreateAccessorNode(Token token, ParseNode node);

        /// <summary>
        /// Create an addition assignment node.
        /// </summary>
        /// <param name="token">The token responsible for this node.</param>
        /// <param name="variableNode">The variable node for the assignment.</param>
        /// <param name="expressionNode">The expression node for the assignment.</param>
        public abstract ParseNode CreateAdditionAssignmentNode(Token token, ParseNode variableNode, ParseNode expressionNode);

        /// <summary>
        /// Create a subtraction assignment node.
        /// </summary>
        /// <param name="token">The token responsible for this node.</param>
        /// <param name="variableNode">The variable node for the assignment.</param>
        /// <param name="expressionNode">The expression node for the assignment.</param>
        public abstract ParseNode CreateSubtractionAssignmentNode(Token token, ParseNode variableNode, ParseNode expressionNode);

        /// <summary>
        /// Create a multiplication assignment node.
        /// </summary>
        /// <param name="token">The token responsible for this node.</param>
        /// <param name="variableNode">The variable node for the assignment.</param>
        /// <param name="expressionNode">The expression node for the assignment.</param>
        public abstract ParseNode CreateMultiplicationAssignmentNode(Token token, ParseNode variableNode, ParseNode expressionNode);

        /// <summary>
        /// Create a division assignment node.
        /// </summary>
        /// <param name="token">The token responsible for this node.</param>
        /// <param name="variableNode">The variable node for the assignment.</param>
        /// <param name="expressionNode">The expression node for the assignment.</param>
        public abstract ParseNode CreateDivisionAssignmentNode(Token token, ParseNode variableNode, ParseNode expressionNode);

        /// <summary>
        /// Create a modulo assignment node.
        /// </summary>
        /// <param name="token">The token responsible for this node.</param>
        /// <param name="variableNode">The variable node for the assignment.</param>
        /// <param name="expressionNode">The expression node for the assignment.</param>
        public abstract ParseNode CreateModuloAssignmentNode(Token token, ParseNode variableNode, ParseNode expressionNode);

        /// <summary>
        /// Create an aggregate node from a list of parse nodes.
        /// </summary>
        /// <param name="nodes">The nodes to aggregate.</param>
        public abstract ParseNode CreateAggregateNode(List<ParseNode> nodes);

        /// <summary>
        /// Create an if control node.
        /// </summary>
        /// <param name="token">The token responsible for this node.</param>
        /// <param name="conditionNode">The if condition node.</param>
        /// <param name="blockNode">The block node.</param>
        public abstract ParseNode CreateIfControlNode(Token token, ParseNode conditionNode, ParseNode blockNode);

        /// <summary>
        /// Create an while control node.
        /// </summary>
        /// <param name="token">The token responsible for this node.</param>
        /// <param name="conditionNode">The while condition node.</param>
        /// <param name="blockNode">The block node.</param>
        public abstract ParseNode CreateWhileControlNode(Token token, ParseNode conditionNode, ParseNode blockNode);

        /// <summary>
        /// Create a foreach loop control node.
        /// </summary>
        /// <param name="token">The token responsible for this node.</param>
        /// <param name="declarationNode">The foreach declaration node.</param>
        /// <param name="variableNode">The variable node to iterate over.</param>
        /// <param name="blockNode">The block to loop.</param>
        public abstract ParseNode CreateForeachControlNode(Token token, ParseNode declarationNode, ParseNode variableNode, ParseNode blockNode);

        /// <summary>
        /// Create a for loop control node.
        /// </summary>
        /// <param name="token">The token responsible for this node.</param>
        /// <param name="declarationNode">The optional declaration node.</param>
        /// <param name="conditionNode">The optional condition node.</param>
        /// <param name="assignmentNode">The optional assignment node.</param>
        /// <param name="blockNode">The block node.</param>
        public abstract ParseNode CreateForControlNode(Token token, ParseNode declarationNode, ParseNode conditionNode, ParseNode assignmentNode, ParseNode blockNode);

        /// <summary>
        /// Create a postfix increment node.
        /// </summary>
        /// <param name="token">The token responsible for this node.</param>
        /// <param name="node">The node we are incrementing.</param>
        public abstract ParseNode CreatePostfixIncrementNode(Token token, ParseNode node);

        /// <summary>
        /// Create a postfix decrement node.
        /// </summary>
        /// <param name="token">The token responsible for this node.</param>
        /// <param name="node">The node we are decrementing.</param>
        public abstract ParseNode CreatePostfixDecrementnode(Token token, ParseNode node);

        /// <summary>
        /// Create a prefix increment node.
        /// </summary>
        /// <param name="token">The token responsible for this node.</param>
        /// <param name="node">The node we are incrementing.</param>
        public abstract ParseNode CreatePrefixIncrementNode(Token token, ParseNode node);

        /// <summary>
        /// Create a prefix decrement node.
        /// </summary>
        /// <param name="token">The token responsible for this node.</param>
        /// <param name="node">The node we are decrementing.</param>
        public abstract ParseNode CreatePrefixDecrementNode(Token token, ParseNode node);

        /// <summary>
        /// Create a constructor node.
        /// </summary>
        /// <param name="token">The token responsible for this node.</param>
        /// <param name="objectSymbol">The symbol for the object we are constructing.</param>
        /// <param name="parameterNodes">The parameters for this constructor.</param>
        public abstract ParseNode CreateConstructorNode(Token token, ObjectSymbol objectSymbol, List<ParseNode> parameterNodes);

        /// <summary>
        /// Create a function node.
        /// </summary>
        /// <param name="token">The token responsible for this node.</param>
        /// <param name="functionSymbol">The symbol for the function.</param>
        public abstract ParseNode CreateFunctionNode(Token token, FunctionSymbol functionSymbol);

        /// <summary>
        /// Create a function call node.
        /// </summary>
        /// <param name="token">The token responsible for this node.</param>
        /// <param name="functionNode">The node for the function to call.</param>
        /// <param name="parameterNodes">The nodes for this call's parameters.</param>
        public abstract ParseNode CreateFunctionCallNode(Token token, ParseNode functionNode, List<ParseNode> parameterNodes);

        /// <summary>
        /// Create an indexer node.
        /// </summary>
        /// <param name="token">The token responsible for this node.</param>
        /// <param name="collectionNode">The node for the collection we are indexing.</param>
        /// <param name="expressionNode">The node for the indexer's expression.</param>
        public abstract ParseNode CreateIndexerCallNode(Token token, ParseNode collectionNode, ParseNode expressionNode);

        /// <summary>
        /// Create a return node.
        /// </summary>
        /// <param name="token">The token responsible for this node.</param>
        public abstract ParseNode CreateReturnNode(Token token);

        /// <summary>
        /// Create a return value node.
        /// </summary>
        /// <param name="token">The token responsible for this node.</param>
        /// <param name="returnExpression">The expression for the value to return.</param>
        public abstract ParseNode CreateReturnValueNode(Token token, ParseNode returnExpression);
        
        /// <summary>
        /// Create a break node.
        /// </summary>
        /// <param name="token">The token responsible for this node.</param>
        public abstract ParseNode CreateBreakNode(Token token);

        /// <summary>
        /// Create an object node.
        /// </summary>
        /// <param name="token">The token responsible for this node.</param>
        /// <param name="symbol">The symbol for this node's object.</param>
        public abstract ParseNode CreateObjectNode(Token token, Symbol symbol);

        /// <summary>
        /// Create an add node for the list object.
        /// </summary>
        public abstract ParseNode CreateListAddNode();

        /// <summary>
        /// Create a remove node for the list object.
        /// </summary>
        public abstract ParseNode CreateListRemoveNode();

        /// <summary>
        /// Create a remove at node for the list object.
        /// </summary>
        public abstract ParseNode CreateListRemoveAtNode();

        /// <summary>
        /// Create a count node for the list object.
        /// </summary>
        public abstract ParseNode CreateListCountNode();

        /// <summary>
        /// Create a contains node for the list object.
        /// </summary>
        public abstract ParseNode CreateListContainsNode();

        /// <summary>
        /// Create a clear node for the list object.
        /// </summary>
        public abstract ParseNode CreateListClearNode();

        /// <summary>
        /// Create a print node for the console object.
        /// </summary>
        public abstract ParseNode CreateConsolePrintNode();

        /// <summary>
        /// Create a print line node for the console object.
        /// </summary>
        public abstract ParseNode CreateConsolePrintLineNode();

        /// <summary>
        /// Create an add node for the dictionary object.
        /// </summary>
        public abstract ParseNode CreateDictionaryAddNode();

        /// <summary>
        /// Create a remove node for the dictionary object.
        /// </summary>
        /// <returns></returns>
        public abstract ParseNode CreateDictionaryRemoveNode();

        /// <summary>
        /// Create a contains key node for the dictionary object.
        /// </summary>
        /// <returns></returns>
        public abstract ParseNode CreateDictionaryContainsKeyNode();

        /// <summary>
        /// Create a contains value node for the dictionary object.
        /// </summary>
        /// <returns></returns>
        public abstract ParseNode CreateDictionaryContainsValueNode();

        /// <summary>
        /// Create a count node for the dictionary object.
        /// </summary>
        /// <returns></returns>
        public abstract ParseNode CreateDictionaryCountNode();

        /// <summary>
        /// Create a clear node for the dictionary object.
        /// </summary>
        /// <returns></returns>
        public abstract ParseNode CreateDictionaryClearNode();

        /// <summary>
        /// Create an add node for the commands object.
        /// </summary>
        public abstract ParseNode CreateCommandsAddNode();

        /// <summary>
        /// Create a remove node for the commands object.
        /// </summary>
        public abstract ParseNode CreateCommandsRemoveNode();
    }
}
