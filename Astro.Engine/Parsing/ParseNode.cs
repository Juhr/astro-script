﻿using Astro.Engine.DataTypes;
using Astro.Engine.Tokenization;

namespace Astro.Engine.Parsing
{
    /// <summary>
    /// Defines the interface for a parse tree node.
    /// </summary>
    public abstract class ParseNode
    {
        /// <summary>
        /// The token responsible for this node.
        /// </summary>
        protected Token mToken;

        /// <summary>
        /// Create the parse node.
        /// </summary>
        /// <param name="token">The token responsible for this node.</param>
        public ParseNode(Token token)
        {
            mToken = token;
        }

        /// <summary>
        /// The token for this parse node.
        /// </summary>
        public Token Token
        {
            get { return mToken; }
        }

        /// <summary>
        /// Execute the behaviour at this node.
        /// </summary>
        /// <returns>The data, if any, that this node returns.</returns>
        public abstract Data Execute();
    }
}
