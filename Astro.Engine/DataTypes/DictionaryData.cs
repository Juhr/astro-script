﻿using System;
using System.Collections.Generic;
using Astro.Engine.ErrorReporting;
using Astro.Engine.Symbols;
using Astro.Engine.Symbols.TypeSymbols;
using Astro.Engine.Tokenization;

namespace Astro.Engine.DataTypes
{
    /// <summary>
    /// The dictionary data type.
    /// </summary>
    public class DictionaryData : ObjectData
    {
        private Dictionary<Data, Data> mDictionary;

        /// <summary>
        /// Create this object data.
        /// </summary>
        /// <param name="listSymbol">The symbol for this object.</param>
        public DictionaryData(DictionarySymbol dictionarySymbol)
            : base(dictionarySymbol)
        {
            // Add the engine list data
            var engineDictionaryData = new EngineDictionaryData();
            mDictionary = engineDictionaryData.Value;
            mVariables.Add("*dictionary", new DataSlot(engineDictionaryData));
        }

        /// <summary>
        /// Index the object with the given value.
        /// </summary>
        /// <param name="index">The index to retrieve data from.</param>
        /// <returns>The data held at the given index.</returns>
        public override Data Index(Data index)
        {
            // Index the dictionary
            try
            {
                return mDictionary[index];
            }
            catch (KeyNotFoundException)
            {
                throw new RuntimeException(new Token(), ErrorCode.KeyNotFound, string.Format("Key '{0}' could not be found in the given 'Dictionary'.", index.ToString()));
            }
        }

        /// <summary>
        /// Get the string representation of this data.
        /// </summary>
        /// <returns>The string representation of this data.</returns>
        public override string ToString()
        {
            return "Dictionary";
        }
    }
}
