﻿using System;
using System.Collections.Generic;
using Astro.Engine.ErrorReporting;
using Astro.Engine.Symbols;
using Astro.Engine.Tokenization;

namespace Astro.Engine.DataTypes
{
    /// <summary>
    /// The object data type.
    /// </summary>
    public class ObjectData : Data
    {
        /// <summary>
        /// The symbol for this object.
        /// </summary>
        protected ObjectSymbol mObjectSymbol;

        /// <summary>
        /// The variables held on this object.
        /// </summary>
        protected Dictionary<string, DataSlot> mVariables;
        
        /// <summary>
        /// Create this object data.
        /// </summary>
        /// <param name="objectSymbol">The symbol for this object.</param>
        public ObjectData(ObjectSymbol objectSymbol)
        {
            mObjectSymbol = objectSymbol;
            mVariables = new Dictionary<string, DataSlot>();

            // Set each member variable
            foreach (var variable in mObjectSymbol.MemberVariables)
            {
                mVariables.Add(variable, new DataSlot());
            }

            // Add the static members
            foreach (var variable in mObjectSymbol.StaticVariables)
            {
                mVariables.Add(variable.Key, variable.Value);
            }
        }

        /// <summary>
        /// Creates a shallow copy of the given object data.
        /// </summary>
        /// <param name="objectData">The object data to copy.</param>
        public ObjectData(ObjectData objectData)
        {
            mObjectSymbol = objectData.Symbol;
            mVariables = objectData.Variables;
        }

        #region Properties

        /// <summary>
        /// The type of this data.
        /// </summary>
        public override DataType Type
        {
            get { return DataType.Object; }
        }

        /// <summary>
        /// The variables of this object. Includes both instance members and static members.
        /// </summary>
        public Dictionary<string, DataSlot> Variables
        {
            get { return mVariables; }
        }

        /// <summary>
        /// The symbol for this object type.
        /// </summary>
        public ObjectSymbol Symbol
        {
            get { return mObjectSymbol; }
        }

        #endregion

        /// <summary>
        /// Create a copy of this data.
        /// </summary>
        /// <returns>A new Data instance with this Data's value.</returns>
        public override Data Copy()
        {
            return new ObjectData(this);
        }

        /// <summary>
        /// Compare this data with another and return whether they are equal.
        /// </summary>
        /// <param name="data">The data to compare with this data.</param>
        /// <returns>The result as a boolean data.</returns>
        public override bool EqualityComparison(Data data)
        {
            return this == data;
        }

        /// <summary>
        /// Index the object with the given value.
        /// </summary>
        /// <param name="index">The index to retrieve data from.</param>
        /// <returns>The data held at the given index.</returns>
        public virtual Data Index(Data index)
        {
            throw new RuntimeException(new Token(), ErrorCode.TypeMismatch, string.Format("Objects of type `{0}' cannot be indexed.", mObjectSymbol.Identifier));
        }

        /// <summary>
        /// Get the string representation of this data.
        /// </summary>
        /// <returns>The string representation of this data.</returns>
        public override string ToString()
        {
            return mObjectSymbol.Identifier;
        }
    }
}
