﻿namespace Astro.Engine.DataTypes
{
    /// <summary>
    /// A data slot used to reference an interchangable item of data.
    /// </summary>
    public class DataSlot
    {
        private Data mData;

        /// <summary>
        /// Create a data slot.
        /// </summary>
        public DataSlot()
        {
            mData = NullData.Instance;
        }

        /// <summary>
        /// Create a new data slot.
        /// </summary>
        /// <param name="data">The data to store in this slot.</param>
        public DataSlot(Data data)
        {
            mData = data;
        }

        /// <summary>
        /// The data at this slot.
        /// </summary>
        public Data Data
        {
            get { return mData; }
            set { mData = value; }
        }
    }
}
