﻿
namespace Astro.Engine.DataTypes
{
    /// <summary>
    /// The float data type.
    /// </summary>
    public class FloatData : Data
    {
        private float mValue;

        /// <summary>
        /// Create the float data type.
        /// </summary>
        /// <param name="value">This float's value.</param>
        public FloatData(float value)
        {
            mValue = value;
        }

        /// <summary>
        /// The type of this data.
        /// </summary>
        public override DataType Type
        {
            get { return DataType.Float; }
        }

        /// <summary>
        /// This float's value.
        /// </summary>
        public float Value
        {
            get { return mValue; }
            set { mValue = value; }
        }

        /// <summary>
        /// Create a copy of this data.
        /// </summary>
        /// <returns>A new Data instance with this Data's value.</returns>
        public override Data Copy()
        {
            return new FloatData(mValue);
        }

        /// <summary>
        /// Compare this data with another and return whether they are equal.
        /// </summary>
        /// <param name="data">The data to compare with this data.</param>
        /// <returns>The result as a boolean data.</returns>
        public override bool EqualityComparison(Data data)
        {
            if (data.Type == DataType.Integer)
            {
                return mValue == ((IntegerData)data).Value;
            }
            else if (data.Type == DataType.Float)
            {
                return mValue == ((FloatData)data).Value;
            }
            return false;
        }

        /// <summary>
        /// Gets the hash code for this data's value.
        /// </summary>
        /// <returns>The data's hash code.</returns>
        public override int GetHashCode()
        {
            return mValue.GetHashCode();
        }

        /// <summary>
        /// Get the string representation of this data.
        /// </summary>
        /// <returns>The string representation of this data.</returns>
        public override string ToString()
        {
            return mValue.ToString();
        }
    }
}
