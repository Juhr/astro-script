﻿namespace Astro.Engine.DataTypes
{
    /// <summary>
    /// The different possible data types.
    /// </summary>
    public enum DataType
    {
        /// <summary>
        /// The null data type.
        /// </summary>
        Null,

        /// <summary>
        /// Integer numeric data type.
        /// </summary>
        Integer,

        /// <summary>
        /// Floating point numeric data type.
        /// </summary>
        Float,

        /// <summary>
        /// String data type.
        /// </summary>
        String,

        /// <summary>
        /// Boolean data type.
        /// </summary>
        Boolean,

        /// <summary>
        /// Object data type.
        /// </summary>
        Object,

        /// <summary>
        /// Function data type.
        /// </summary>
        Function,

        /// <summary>
        /// Data type reserved for engine use only.
        /// </summary>
        Engine,
    }
}
