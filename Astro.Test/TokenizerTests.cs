﻿using Astro.Engine.ErrorReporting;
using Astro.Engine.Interpreter;
using Astro.Engine.Symbols;
using Astro.Engine.Tokenization;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Astro.Test
{
    /// <summary>
    /// Runs tests on the tokenizer part of the engine.
    /// </summary>
    [TestClass]
    public class TokenizerTests
    {
        /// <summary>
        /// Tests that tokenizing an empty source string returns an
        /// empty token stream.
        /// </summary>
        [TestMethod]
        public void EmptySource()
        {
            var stream = this.TokenizeSource("");
            Assert.IsTrue(stream.Count == 1);

            var token = stream.Read();
            this.AssetToken(token, "", TokenType.EndOfStream, 0, 0);
        }

        /// <summary>
        /// Tests that names are correctly tokenized.
        /// </summary>
        [TestMethod]
        public void ReadNames()
        {
            var stream = this.TokenizeSource("hello these are some names");

            // First token
            var token = stream.Tokens[0];
            Assert.AreEqual(token.Lexeme, "hello");
            Assert.AreEqual(token.Type, TokenType.Name);
            Assert.AreEqual(token.StartPosition, 0);
            Assert.AreEqual(token.EndPosition, 5);

            // Second token
            token = stream.Tokens[1];
            Assert.AreEqual(token.Lexeme, "these");
            Assert.AreEqual(token.Type, TokenType.Name);
            Assert.AreEqual(token.StartPosition, 6);
            Assert.AreEqual(token.EndPosition, 11);

            // Third token
            token = stream.Tokens[2];
            Assert.AreEqual(token.Lexeme, "are");
            Assert.AreEqual(token.Type, TokenType.Name);
            Assert.AreEqual(token.StartPosition, 12);
            Assert.AreEqual(token.EndPosition, 15);

            // Fourth token
            token = stream.Tokens[3];
            Assert.AreEqual(token.Lexeme, "some");
            Assert.AreEqual(token.Type, TokenType.Name);
            Assert.AreEqual(token.StartPosition, 16);
            Assert.AreEqual(token.EndPosition, 20);

            // Fifth token
            token = stream.Tokens[4];
            Assert.AreEqual(token.Lexeme, "names");
            Assert.AreEqual(token.Type, TokenType.Name);
            Assert.AreEqual(token.StartPosition, 21);
            Assert.AreEqual(token.EndPosition, 26);
        }

        /// <summary>
        /// Tests that numbers are correctly tokenized.
        /// </summary>
        [TestMethod]
        public void ReadNumbers()
        {
            var stream = this.TokenizeSource("123 1.0 738473628.7837292837");

            // First token
            var token = stream.Tokens[0];
            Assert.AreEqual(token.Lexeme, "123");
            Assert.AreEqual(token.Type, TokenType.NumericLiteral);
            Assert.AreEqual(token.StartPosition, 0);
            Assert.AreEqual(token.EndPosition, 3);

            // Second token
            token = stream.Tokens[1];
            Assert.AreEqual(token.Lexeme, "1.0");
            Assert.AreEqual(token.Type, TokenType.NumericLiteral);
            Assert.AreEqual(token.StartPosition, 4);
            Assert.AreEqual(token.EndPosition, 7);

            // Third token
            token = stream.Tokens[2];
            Assert.AreEqual(token.Lexeme, "738473628.7837292837");
            Assert.AreEqual(token.Type, TokenType.NumericLiteral);
            Assert.AreEqual(token.StartPosition, 8);
            Assert.AreEqual(token.EndPosition, 28);
        }

        /// <summary>
        /// Tests that numbers with a decimal part but no decimal number throw
        /// an exception.
        /// </summary>
        [TestMethod]
        public void InvalidDecimalPart()
        {
            var errorLog = new ErrorLog();
            var stream = this.TokenizeSource("123 1. 738473628.7837292837", true, errorLog);

            Assert.AreEqual(ErrorCode.MissingFractionalNumbers, errorLog.Entries[0].ErrorCode);
        }

        /// <summary>
        /// Tests that implicit end of statements are read correctly.
        /// </summary>
        [TestMethod]
        public void ImplicitEndOfStatements()
        {
            var stream = this.TokenizeSource("var name = 12\nvar help = \"Hello\" \n Oh My");

            // Fifth token
            var token = stream.Tokens[4];
            Assert.AreEqual(token.Lexeme, "\n");
            Assert.AreEqual(token.Type, TokenType.ImplicitEndOfStatement);
            Assert.AreEqual(token.StartPosition, 13);
            Assert.AreEqual(token.EndPosition, 14);

            // Tenth token
            token = stream.Tokens[9];
            Assert.AreEqual(token.Lexeme, "\n");
            Assert.AreEqual(token.Type, TokenType.ImplicitEndOfStatement);
            Assert.AreEqual(token.StartPosition, 33);
            Assert.AreEqual(token.EndPosition, 34);
        }

        /// <summary>
        /// Tests that explicit end of statements are read correctly.
        /// </summary>
        [TestMethod]
        public void ExplicitEndOfStatements()
        {
            var stream = this.TokenizeSource("var name = 12;var help = \"Hello\" ; Oh My");

            // Fifth token
            var token = stream.Tokens[4];
            Assert.AreEqual(token.Lexeme, ";");
            Assert.AreEqual(token.Type, TokenType.EndOfStatement);
            Assert.AreEqual(token.StartPosition, 13);
            Assert.AreEqual(token.EndPosition, 14);

            // Tenth token
            token = stream.Tokens[9];
            Assert.AreEqual(token.Lexeme, ";");
            Assert.AreEqual(token.Type, TokenType.EndOfStatement);
            Assert.AreEqual(token.StartPosition, 33);
            Assert.AreEqual(token.EndPosition, 34);
        }

        /// <summary>
        /// Tests that assignment operators are read correctly.
        /// </summary>
        [TestMethod]
        public void AssignmentOperators()
        {
            var stream = this.TokenizeSource("= += -= /= *= %=");

            // First token
            var token = stream.Tokens[0];
            Assert.AreEqual(token.Lexeme, "=");
            Assert.AreEqual(token.Type, TokenType.Assignment);
            Assert.AreEqual(token.StartPosition, 0);
            Assert.AreEqual(token.EndPosition, 1);

            // Second token
            token = stream.Tokens[1];
            Assert.AreEqual(token.Lexeme, "+=");
            Assert.AreEqual(token.Type, TokenType.AdditionAssignment);
            Assert.AreEqual(token.StartPosition, 2);
            Assert.AreEqual(token.EndPosition, 4);

            // Third token
            token = stream.Tokens[2];
            Assert.AreEqual(token.Lexeme, "-=");
            Assert.AreEqual(token.Type, TokenType.SubtractionAssignment);
            Assert.AreEqual(token.StartPosition, 5);
            Assert.AreEqual(token.EndPosition, 7);

            // Fourth token
            token = stream.Tokens[3];
            Assert.AreEqual(token.Lexeme, "/=");
            Assert.AreEqual(token.Type, TokenType.DivisionAssignment);
            Assert.AreEqual(token.StartPosition, 8);
            Assert.AreEqual(token.EndPosition, 10);

            // Fifth token
            token = stream.Tokens[4];
            Assert.AreEqual(token.Lexeme, "*=");
            Assert.AreEqual(token.Type, TokenType.MultiplicationAssignment);
            Assert.AreEqual(token.StartPosition, 11);
            Assert.AreEqual(token.EndPosition, 13);

            // Sixth token
            token = stream.Tokens[5];
            Assert.AreEqual(token.Lexeme, "%=");
            Assert.AreEqual(token.Type, TokenType.ModuloAssignment);
            Assert.AreEqual(token.StartPosition, 14);
            Assert.AreEqual(token.EndPosition, 16);
        }

        /// <summary>
        /// Tests that string literals are tokenized correctly.
        /// </summary>
        [TestMethod]
        public void StringLiterals()
        {
            var stream = this.TokenizeSource(" \"Hello World!\" \"123.123 var string literal\" ");

            // First token
            var token = stream.Tokens[0];
            Assert.AreEqual(token.Lexeme, "\"Hello World!\"");
            Assert.AreEqual(token.Type, TokenType.StringLiteral);
            Assert.AreEqual(token.StartPosition, 1);
            Assert.AreEqual(token.EndPosition, 15);

            // Second token
            token = stream.Tokens[1];
            Assert.AreEqual(token.Lexeme, "\"123.123 var string literal\"");
            Assert.AreEqual(token.Type, TokenType.StringLiteral);
            Assert.AreEqual(token.StartPosition, 16);
            Assert.AreEqual(token.EndPosition, 44);
        }

        /// <summary>
        /// Tests that code with trailing whitespace tokenizes properly.
        /// </summary>
        [TestMethod]
        public void TrailingWhitespace()
        {
            var stream = this.TokenizeSource("Hello    ");

            // First token
            var token = stream.Tokens[0];
            this.AssetToken(token, "Hello", TokenType.Name, 0, 5);

            // End of stream token
            token = stream.Tokens[1];
            this.AssetToken(token, "", TokenType.EndOfStream, 9, 9);

            Assert.IsTrue(stream.Tokens.Count == 2);
        }

        /// <summary>
        /// Tests that blocks enclosed by braces are tokenized correctly, along with
        /// their contents.
        /// </summary>
        [TestMethod]
        public void ReadBraceBlock()
        {
            var stream = this.TokenizeSource("{ var correct = true }");

            // Block start token
            var token = stream.Tokens[0];
            Assert.AreEqual(token.Lexeme, "{");
            Assert.AreEqual(token.Type, TokenType.BraceBlockStart);
            Assert.AreEqual(token.StartPosition, 0);
            Assert.AreEqual(token.EndPosition, 1);

            // Second token
            token = stream.Tokens[1];
            Assert.AreEqual(token.Lexeme, "var");
            Assert.AreEqual(token.Type, TokenType.Declaration);
            Assert.AreEqual(token.StartPosition, 2);
            Assert.AreEqual(token.EndPosition, 5);

            // Third token
            token = stream.Tokens[2];
            Assert.AreEqual(token.Lexeme, "correct");
            Assert.AreEqual(token.Type, TokenType.Name);
            Assert.AreEqual(token.StartPosition, 6);
            Assert.AreEqual(token.EndPosition, 13);

            // Fourth token
            token = stream.Tokens[3];
            Assert.AreEqual(token.Lexeme, "=");
            Assert.AreEqual(token.Type, TokenType.Assignment);
            Assert.AreEqual(token.StartPosition, 14);
            Assert.AreEqual(token.EndPosition, 15);

            // Fifth token
            token = stream.Tokens[4];
            Assert.AreEqual(token.Lexeme, "true");
            Assert.AreEqual(token.Type, TokenType.True);
            Assert.AreEqual(token.StartPosition, 16);
            Assert.AreEqual(token.EndPosition, 20);

            // Block end token
            token = stream.Tokens[5];
            Assert.AreEqual(token.Lexeme, "}");
            Assert.AreEqual(token.Type, TokenType.BraceBlockEnd);
            Assert.AreEqual(token.StartPosition, 21);
            Assert.AreEqual(token.EndPosition, 22);

        }

        /// <summary>
        /// Tests that an exception is thrown when a brace block is missing
        /// its closing brace.
        /// </summary>
        [TestMethod]
        public void BraceBlockMissingClosingBrace()
        {
            var errorLog = new ErrorLog();
            var stream = this.TokenizeSource("{ var correct = true ", true, errorLog);

            Assert.AreEqual(ErrorCode.ClosingBraceMissing, errorLog.Entries[0].ErrorCode);
        }

        /// <summary>
        /// Tests that an exception is thrown when a brace block is missing
        /// its opening brace.
        /// </summary>
        [TestMethod]
        public void BraceBlockMissingOpeningBrace()
        {
            var errorLog = new ErrorLog();
            var stream = this.TokenizeSource(" var correct = true }", true, errorLog);

            Assert.AreEqual(ErrorCode.OpeningBraceMissing, errorLog.Entries[0].ErrorCode);
        }

        /// <summary>
        /// Tests that parenthesis blocks are tokenized correctly.
        /// </summary>
        [TestMethod]
        public void ReadParenthesisBlock()
        {
            var stream = this.TokenizeSource("( var correct = true )");

            // Block start token
            var token = stream.Tokens[0];
            Assert.AreEqual(token.Lexeme, "(");
            Assert.AreEqual(token.Type, TokenType.ParenthesisBlockStart);
            Assert.AreEqual(token.StartPosition, 0);
            Assert.AreEqual(token.EndPosition, 1);

            // Second token
            token = stream.Tokens[1];
            Assert.AreEqual(token.Lexeme, "var");
            Assert.AreEqual(token.Type, TokenType.Declaration);
            Assert.AreEqual(token.StartPosition, 2);
            Assert.AreEqual(token.EndPosition, 5);

            // Third token
            token = stream.Tokens[2];
            Assert.AreEqual(token.Lexeme, "correct");
            Assert.AreEqual(token.Type, TokenType.Name);
            Assert.AreEqual(token.StartPosition, 6);
            Assert.AreEqual(token.EndPosition, 13);

            // Fourth token
            token = stream.Tokens[3];
            Assert.AreEqual(token.Lexeme, "=");
            Assert.AreEqual(token.Type, TokenType.Assignment);
            Assert.AreEqual(token.StartPosition, 14);
            Assert.AreEqual(token.EndPosition, 15);

            // Fifth token
            token = stream.Tokens[4];
            Assert.AreEqual(token.Lexeme, "true");
            Assert.AreEqual(token.Type, TokenType.True);
            Assert.AreEqual(token.StartPosition, 16);
            Assert.AreEqual(token.EndPosition, 20);

            // Block end token
            token = stream.Tokens[5];
            Assert.AreEqual(token.Lexeme, ")");
            Assert.AreEqual(token.Type, TokenType.ParenthesisBlockEnd);
            Assert.AreEqual(token.StartPosition, 21);
            Assert.AreEqual(token.EndPosition, 22);

        }

        /// <summary>
        /// Tests that an exception is thrown when a parenthesis block is missing
        /// its closing parenthesis.
        /// </summary>
        [TestMethod]
        public void ParenthesisBlockMissingClosingParenthesis()
        {
            var errorLog = new ErrorLog();
            var stream = this.TokenizeSource("( var correct = true ", true, errorLog);

            Assert.AreEqual(ErrorCode.ClosingParenthesisMissing, errorLog.Entries[0].ErrorCode);
        }

        /// <summary>
        /// Tests that an exception is thrown when a parenthesis block is missing
        /// its opening parenthesis.
        /// </summary>
        [TestMethod]
        public void ParenthesisBlockMissingOpeningParenthesis()
        {
            var errorLog = new ErrorLog();
            var stream = this.TokenizeSource(" var correct = true )", true, errorLog);

            Assert.AreEqual(ErrorCode.OpeningParenthesisMissing, errorLog.Entries[0].ErrorCode);
        }

        /// <summary>
        /// Tests that bracket blocks are tokenized correctly.
        /// </summary>
        [TestMethod]
        public void ReadBracketBlock()
        {
            var stream = this.TokenizeSource("[ var correct = true ]");

            // Block start token
            var token = stream.Tokens[0];
            Assert.AreEqual(token.Lexeme, "[");
            Assert.AreEqual(token.Type, TokenType.BracketBlockStart);
            Assert.AreEqual(token.StartPosition, 0);
            Assert.AreEqual(token.EndPosition, 1);

            // Second token
            token = stream.Tokens[1];
            Assert.AreEqual(token.Lexeme, "var");
            Assert.AreEqual(token.Type, TokenType.Declaration);
            Assert.AreEqual(token.StartPosition, 2);
            Assert.AreEqual(token.EndPosition, 5);

            // Third token
            token = stream.Tokens[2];
            Assert.AreEqual(token.Lexeme, "correct");
            Assert.AreEqual(token.Type, TokenType.Name);
            Assert.AreEqual(token.StartPosition, 6);
            Assert.AreEqual(token.EndPosition, 13);

            // Fourth token
            token = stream.Tokens[3];
            Assert.AreEqual(token.Lexeme, "=");
            Assert.AreEqual(token.Type, TokenType.Assignment);
            Assert.AreEqual(token.StartPosition, 14);
            Assert.AreEqual(token.EndPosition, 15);

            // Fifth token
            token = stream.Tokens[4];
            Assert.AreEqual(token.Lexeme, "true");
            Assert.AreEqual(token.Type, TokenType.True);
            Assert.AreEqual(token.StartPosition, 16);
            Assert.AreEqual(token.EndPosition, 20);

            // Block start token
            token = stream.Tokens[5];
            Assert.AreEqual(token.Lexeme, "]");
            Assert.AreEqual(token.Type, TokenType.BracketBlockEnd);
            Assert.AreEqual(token.StartPosition, 21);
            Assert.AreEqual(token.EndPosition, 22);

        }

        /// <summary>
        /// Tests that an exception is thrown when a bracket block is missing
        /// its closing bracket.
        /// </summary>
        [TestMethod]
        public void BracketBlockMissingClosingBracket()
        {
            var errorLog = new ErrorLog();
            var stream = this.TokenizeSource("[ var correct = true ", true, errorLog);

            Assert.AreEqual(ErrorCode.ClosingBracketMissing, errorLog.Entries[0].ErrorCode);
        }

        /// <summary>
        /// Tests that an exception is thrown when a bracket block is missing
        /// its opening bracket.
        /// </summary>
        [TestMethod]
        public void BracketsBlockMissingOpeningBracket()
        {
            var errorLog = new ErrorLog();
            var stream = this.TokenizeSource(" var correct = true ]", true, errorLog);

            Assert.AreEqual(ErrorCode.OpeningBracketMissing, errorLog.Entries[0].ErrorCode);
        }

        /// <summary>
        /// Tests whether nested brace blocks are read correctly.
        /// </summary>
        [TestMethod]
        public void ReadNestedBraceBlocks()
        {
            var stream = this.TokenizeSource("{{{help}}}");

            // First block start token
            var token = stream.Tokens[0];
            Assert.AreEqual(token.Lexeme, "{");
            Assert.AreEqual(token.Type, TokenType.BraceBlockStart);
            Assert.AreEqual(token.StartPosition, 0);
            Assert.AreEqual(token.EndPosition, 1);

            // Second block token
            token = stream.Tokens[1];
            Assert.AreEqual(token.Lexeme, "{");
            Assert.AreEqual(token.Type, TokenType.BraceBlockStart);
            Assert.AreEqual(token.StartPosition, 1);
            Assert.AreEqual(token.EndPosition, 2);

            // Third block token
            token = stream.Tokens[2];
            Assert.AreEqual(token.Lexeme, "{");
            Assert.AreEqual(token.Type, TokenType.BraceBlockStart);
            Assert.AreEqual(token.StartPosition, 2);
            Assert.AreEqual(token.EndPosition, 3);

            // Token at the centre
            token = stream.Tokens[3];
            Assert.AreEqual(token.Lexeme, "help");
            Assert.AreEqual(token.Type, TokenType.Name);
            Assert.AreEqual(token.StartPosition, 3);
            Assert.AreEqual(token.EndPosition, 7);

            // Third block end token
            token = stream.Tokens[4];
            Assert.AreEqual(token.Lexeme, "}");
            Assert.AreEqual(token.Type, TokenType.BraceBlockEnd);
            Assert.AreEqual(token.StartPosition, 7);
            Assert.AreEqual(token.EndPosition, 8);

            // Second block token
            token = stream.Tokens[5];
            Assert.AreEqual(token.Lexeme, "}");
            Assert.AreEqual(token.Type, TokenType.BraceBlockEnd);
            Assert.AreEqual(token.StartPosition, 8);
            Assert.AreEqual(token.EndPosition, 9);

            // First block token
            token = stream.Tokens[6];
            Assert.AreEqual(token.Lexeme, "}");
            Assert.AreEqual(token.Type, TokenType.BraceBlockEnd);
            Assert.AreEqual(token.StartPosition, 9);
            Assert.AreEqual(token.EndPosition, 10);
        }

        /// <summary>
        /// Tests whether nested parenthesis blocks are read correctly.
        /// </summary>
        [TestMethod]
        public void ReadNestedParenthesisBlocks()
        {
            var stream = this.TokenizeSource("(((help)))");

            // First block start token
            var token = stream.Tokens[0];
            Assert.AreEqual(token.Lexeme, "(");
            Assert.AreEqual(token.Type, TokenType.ParenthesisBlockStart);
            Assert.AreEqual(token.StartPosition, 0);
            Assert.AreEqual(token.EndPosition, 1);

            // Second block token
            token = stream.Tokens[1];
            Assert.AreEqual(token.Lexeme, "(");
            Assert.AreEqual(token.Type, TokenType.ParenthesisBlockStart);
            Assert.AreEqual(token.StartPosition, 1);
            Assert.AreEqual(token.EndPosition, 2);

            // Third block token
            token = stream.Tokens[2];
            Assert.AreEqual(token.Lexeme, "(");
            Assert.AreEqual(token.Type, TokenType.ParenthesisBlockStart);
            Assert.AreEqual(token.StartPosition, 2);
            Assert.AreEqual(token.EndPosition, 3);

            // Token at the centre
            token = stream.Tokens[3];
            Assert.AreEqual(token.Lexeme, "help");
            Assert.AreEqual(token.Type, TokenType.Name);
            Assert.AreEqual(token.StartPosition, 3);
            Assert.AreEqual(token.EndPosition, 7);

            // Third block end token
            token = stream.Tokens[4];
            Assert.AreEqual(token.Lexeme, ")");
            Assert.AreEqual(token.Type, TokenType.ParenthesisBlockEnd);
            Assert.AreEqual(token.StartPosition, 7);
            Assert.AreEqual(token.EndPosition, 8);

            // Second block token
            token = stream.Tokens[5];
            Assert.AreEqual(token.Lexeme, ")");
            Assert.AreEqual(token.Type, TokenType.ParenthesisBlockEnd);
            Assert.AreEqual(token.StartPosition, 8);
            Assert.AreEqual(token.EndPosition, 9);

            // First block token
            token = stream.Tokens[6];
            Assert.AreEqual(token.Lexeme, ")");
            Assert.AreEqual(token.Type, TokenType.ParenthesisBlockEnd);
            Assert.AreEqual(token.StartPosition, 9);
            Assert.AreEqual(token.EndPosition, 10);
        }

        /// <summary>
        /// Tests whether nested bracket blocks are read correctly.
        /// </summary>
        [TestMethod]
        public void ReadNestedBracketBlocks()
        {
            var stream = this.TokenizeSource("[[[help]]]");

            // First block start token
            var token = stream.Tokens[0];
            Assert.AreEqual(token.Lexeme, "[");
            Assert.AreEqual(token.Type, TokenType.BracketBlockStart);
            Assert.AreEqual(token.StartPosition, 0);
            Assert.AreEqual(token.EndPosition, 1);

            // Second block token
            token = stream.Tokens[1];
            Assert.AreEqual(token.Lexeme, "[");
            Assert.AreEqual(token.Type, TokenType.BracketBlockStart);
            Assert.AreEqual(token.StartPosition, 1);
            Assert.AreEqual(token.EndPosition, 2);

            // Third block token
            token = stream.Tokens[2];
            Assert.AreEqual(token.Lexeme, "[");
            Assert.AreEqual(token.Type, TokenType.BracketBlockStart);
            Assert.AreEqual(token.StartPosition, 2);
            Assert.AreEqual(token.EndPosition, 3);

            // Token at the centre
            token = stream.Tokens[3];
            Assert.AreEqual(token.Lexeme, "help");
            Assert.AreEqual(token.Type, TokenType.Name);
            Assert.AreEqual(token.StartPosition, 3);
            Assert.AreEqual(token.EndPosition, 7);

            // Third block end token
            token = stream.Tokens[4];
            Assert.AreEqual(token.Lexeme, "]");
            Assert.AreEqual(token.Type, TokenType.BracketBlockEnd);
            Assert.AreEqual(token.StartPosition, 7);
            Assert.AreEqual(token.EndPosition, 8);

            // Second block token
            token = stream.Tokens[5];
            Assert.AreEqual(token.Lexeme, "]");
            Assert.AreEqual(token.Type, TokenType.BracketBlockEnd);
            Assert.AreEqual(token.StartPosition, 8);
            Assert.AreEqual(token.EndPosition, 9);

            // First block token
            token = stream.Tokens[6];
            Assert.AreEqual(token.Lexeme, "]");
            Assert.AreEqual(token.Type, TokenType.BracketBlockEnd);
            Assert.AreEqual(token.StartPosition, 9);
            Assert.AreEqual(token.EndPosition, 10);
        }

        /// <summary>
        /// Tests that the equals comparison is read correctly.
        /// </summary>
        [TestMethod]
        public void ReadEqualsComparison()
        {
            var stream = this.TokenizeSource("true == true");

            // Second token
            var token = stream.Tokens[1];
            Assert.AreEqual(token.Lexeme, "==");
            Assert.AreEqual(token.Type, TokenType.EqualsComparision);
            Assert.AreEqual(token.StartPosition, 5);
            Assert.AreEqual(token.EndPosition, 7);
        }

        /// <summary>
        /// Tests that the not equal comparison is read correctly.
        /// </summary>
        [TestMethod]
        public void ReadNotEqualComparison()
        {
            var stream = this.TokenizeSource("true != true");

            // Second token
            var token = stream.Tokens[1];
            Assert.AreEqual(token.Lexeme, "!=");
            Assert.AreEqual(token.Type, TokenType.NotEqualComparison);
            Assert.AreEqual(token.StartPosition, 5);
            Assert.AreEqual(token.EndPosition, 7);
        }

        /// <summary>
        /// Tests that the greater than comparison is read correctly.
        /// </summary>
        [TestMethod]
        public void ReadGreaterThanComparison()
        {
            var stream = this.TokenizeSource("1 > 1");

            // Second token
            var token = stream.Tokens[1];
            Assert.AreEqual(token.Lexeme, ">");
            Assert.AreEqual(token.Type, TokenType.GreaterThanComparison);
            Assert.AreEqual(token.StartPosition, 2);
            Assert.AreEqual(token.EndPosition, 3);
        }

        /// <summary>
        /// Tests that the less than comparison is read correctly.
        /// </summary>
        [TestMethod]
        public void ReadLessThanComparison()
        {
            var stream = this.TokenizeSource("1 < 1");

            // Second token
            var token = stream.Tokens[1];
            Assert.AreEqual(token.Lexeme, "<");
            Assert.AreEqual(token.Type, TokenType.LessThanComparison);
            Assert.AreEqual(token.StartPosition, 2);
            Assert.AreEqual(token.EndPosition, 3);
        }

        /// <summary>
        /// Tests that the greater than or equal to comparison is read correctly.
        /// </summary>
        [TestMethod]
        public void ReadGreaterThanOrEqualToComparison()
        {
            var stream = this.TokenizeSource("1 >= 1");

            // Second token
            var token = stream.Tokens[1];
            Assert.AreEqual(token.Lexeme, ">=");
            Assert.AreEqual(token.Type, TokenType.GreaterThanOrEqualToComparison);
            Assert.AreEqual(token.StartPosition, 2);
            Assert.AreEqual(token.EndPosition, 4);
        }

        /// <summary>
        /// Tests that the less than or equal to comparison is read correctly.
        /// </summary>
        [TestMethod]
        public void ReadLessThanOrEqualToComparison()
        {
            var stream = this.TokenizeSource("1 <= 1");

            // Second token
            var token = stream.Tokens[1];
            Assert.AreEqual(token.Lexeme, "<=");
            Assert.AreEqual(token.Type, TokenType.LessThanOrEqualToComparison);
            Assert.AreEqual(token.StartPosition, 2);
            Assert.AreEqual(token.EndPosition, 4);
        }

        /// <summary>
        /// Tests that operators are read correctly.
        /// </summary>
        [TestMethod]
        public void ReadOperators()
        {
            var stream = this.TokenizeSource("1 + 1, 2 - 2, 3 * 3, 4 / 4, 5 % 5");

            // First operator
            var token = stream.Tokens[1];
            this.AssetToken(token, "+", TokenType.Addition, 2, 3);

            // Second operator
            token = stream.Tokens[3];
            this.AssetToken(token, ",", TokenType.Comma, 5, 6);

            // Third operator
            token = stream.Tokens[5];
            this.AssetToken(token, "-", TokenType.Subtraction, 9, 10);

            // Fourth operator
            token = stream.Tokens[7];
            this.AssetToken(token, ",", TokenType.Comma, 12, 13);

            // Fifth operator
            token = stream.Tokens[9];
            this.AssetToken(token, "*", TokenType.Multiplication, 16, 17);

            // Sixth operator
            token = stream.Tokens[11];
            this.AssetToken(token, ",", TokenType.Comma, 19, 20);

            // Seventh operator
            token = stream.Tokens[13];
            this.AssetToken(token, "/", TokenType.Division, 23, 24);

            // Eigth operator
            token = stream.Tokens[15];
            this.AssetToken(token, ",", TokenType.Comma, 26, 27);

            // Ninth operator
            token = stream.Tokens[17];
            this.AssetToken(token, "%", TokenType.Modulo, 30, 31);
        }

        /// <summary>
        /// Tests that the short circuiting and normal boolean and operators are read correctly.
        /// </summary>
        [TestMethod]
        public void ReadBooleanAndOperators()
        {
            var stream = this.TokenizeSource("true && true, true & true");

            // First operator
            var token = stream.Tokens[1];
            this.AssetToken(token, "&&", TokenType.ConditionalAnd, 5, 7);

            // Second operator
            token = stream.Tokens[5];
            this.AssetToken(token, "&", TokenType.LogicalAnd, 19, 20);
        }

        /// <summary>
        /// Tests that the short circuiting and normal boolean or operators are read correctly.
        /// </summary>
        [TestMethod]
        public void ReadBooleanOrOperators()
        {
            var stream = this.TokenizeSource("true || true, true | true");

            // First operator
            var token = stream.Tokens[1];
            this.AssetToken(token, "||", TokenType.ConditionalOr, 5, 7);

            // Second operator
            token = stream.Tokens[5];
            this.AssetToken(token, "|", TokenType.LogicalOr, 19, 20);
        }

        /// <summary>
        /// Ensure that the numeric negation operator is read correctly.
        /// </summary>
        [TestMethod]
        public void ReadNumericNegation()
        {
            var stream = this.TokenizeSource("var a = 5 - -1 (12) -6");

            // Ensure the subtraction operator is treated as such
            var token = stream.Tokens[4];
            this.AssetToken(token, "-", TokenType.Subtraction, 10, 11);

            // Ensure the negation operator is treated as such
            token = stream.Tokens[5];
            this.AssetToken(token, "-", TokenType.NumericNegation, 12, 13);

            // Ensure the second subtraction operator is treated as such
            token = stream.Tokens[10];
            this.AssetToken(token, "-", TokenType.Subtraction, 20, 21);
        }

        /// <summary>
        /// Ensure that the boolean negation operator is read correctly
        /// </summary>
        [TestMethod]
        public void ReadBooleanNegation()
        {
            var stream = this.TokenizeSource("a != !b || !(a && b)");

            // Ensure the not equal comparison is treated as such
            var token = stream.Tokens[1];
            this.AssetToken(token, "!=", TokenType.NotEqualComparison, 2, 4);

            // Ensure the negation operator is treated as such
            token = stream.Tokens[2];
            this.AssetToken(token, "!", TokenType.BooleanNegation, 5, 6);

            // Ensure the second negation operator is treated as such
            token = stream.Tokens[5];
            this.AssetToken(token, "!", TokenType.BooleanNegation, 11, 12);
        }

        /// <summary>
        /// Test that the increment and decrement operators are read correctly.
        /// </summary>
        [TestMethod]
        public void ReadIncrementDecrement()
        {
            var stream = this.TokenizeSource("++a-- --b++");

            // Ensure the prefix increment is treated as such
            var token = stream.Tokens[0];
            this.AssetToken(token, "++", TokenType.Increment, 0, 2);

            // Ensure the postfix decrement is treated as such
            token = stream.Tokens[2];
            this.AssetToken(token, "--", TokenType.Decrement, 3, 5);

            // Ensure the prefix decrement is treated as such
            token = stream.Tokens[3];
            this.AssetToken(token, "--", TokenType.Decrement, 6, 8);

            // Ensure the postfix increment is treated as such
            token = stream.Tokens[5];
            this.AssetToken(token, "++", TokenType.Increment, 9, 11);
        }

        /// <summary>
        /// Test that a member accessor is read properly.
        /// </summary>
        [TestMethod]
        public void ReadMemberAccessor()
        {
            var stream = this.TokenizeSource("this.that.theother.what()");

            // First member accessor
            var token = stream.Tokens[1];
            this.AssetToken(token, ".", TokenType.MemberAccessor, 4, 5);

            // Second accessor
            token = stream.Tokens[3];
            this.AssetToken(token, ".", TokenType.MemberAccessor, 9, 10);

            // Third accessor
            token = stream.Tokens[5];
            this.AssetToken(token, ".", TokenType.MemberAccessor, 18, 19);
        }

        /// <summary>
        /// Test that a null token is read correctly.
        /// </summary>
        [TestMethod]
        public void ReadNull()
        {
            var stream = this.TokenizeSource("var a = null");

            // Null token
            var token = stream.Tokens[3];
            this.AssetToken(token, "null", TokenType.Null, 8, 12);
        }

        /// <summary>
        /// Test that break tokens are read properly.
        /// </summary>
        [TestMethod]
        public void ReadBreak()
        {
            var stream = this.TokenizeSource("while (true) { break }");

            // Break token
            var token = stream.Tokens[5];
            this.AssetToken(token, "break", TokenType.Break, 15, 20);
        }

        /// <summary>
        /// Test that the static keyword is read properly.
        /// </summary>
        [TestMethod]
        public void ReadStatic()
        {
            var stream = this.TokenizeSource("static var helloWorld");

            // Static token
            var token = stream.Tokens[0];
            this.AssetToken(token, "static", TokenType.Static, 0, 6);
        }

        /// <summary>
        /// Assert that a token has the expected features.
        /// </summary>
        /// <param name="token">The token we are checking.</param>
        /// <param name="lexeme">The lexeme we expect.</param>
        /// <param name="type">The type we expect.</param>
        /// <param name="startPosition">The start position we expect.</param>
        /// <param name="endPosition">The end position we expect.</param>
        private void AssetToken(Token token, string lexeme, TokenType type, int startPosition, int endPosition)
        {
            Assert.AreEqual(lexeme, token.Lexeme);
            Assert.AreEqual(type, token.Type);
            Assert.AreEqual(startPosition, token.StartPosition);
            Assert.AreEqual(endPosition, token.EndPosition);
        }

        /// <summary>
        /// Helper method to automatically set up required variables for
        /// tokenization.
        /// </summary>
        /// <param name="source">The source code to tokenize.</param>
        private TokenStream TokenizeSource(string source, bool errorsExpected = false, ErrorLog errorLog = null)
        {
            var nodeFactory = new InterpreterParseNodeFactory();
            var symbolTable = new SymbolTable(nodeFactory);
            if (errorLog == null)
            {
                errorLog = new ErrorLog();
            }
            TokenStream stream;
            Assert.AreEqual(!errorsExpected, new Tokenizer(symbolTable, errorLog).Tokenize(source, out stream));
            return stream;
        }
    }
}