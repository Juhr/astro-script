﻿using System.Collections.Generic;
using System.Text;
using Astro.Engine.DataTypes;
using Astro.Engine.ErrorReporting;
using Astro.Engine.Interpreter;
using Astro.Engine.Interpreter.ExecutionEnvironment;
using Astro.Engine.Parsing;
using Astro.Engine.Symbols;
using Astro.Engine.Tokenization;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Astro.Test
{
    /// <summary>
    /// Runs tests on Astro's interpreter.
    /// </summary>
    [TestClass]
    public class InterpreterTests
    {
        private SymbolTable mSymbolTable;
        private ErrorLog mErrorLog;
        private Tokenizer mTokenizer;
        private InterpreterParseNodeFactory mNodeFactory;
        private InterpreterExecutionEnvironment mExecutionEnvironment;
        private Parser mParser;
        private TestCommandsEventListener mEventListener;

        /// <summary>
        /// Initialise the variables for each test.
        /// </summary>
        [TestInitialize]
        public void Initialise()
        {
            mEventListener = new TestCommandsEventListener();
            mNodeFactory = new InterpreterParseNodeFactory();
            mErrorLog = new ErrorLog();
            mSymbolTable = new SymbolTable(mNodeFactory);
            mTokenizer = new Tokenizer(mSymbolTable, mErrorLog);
            mParser = new Parser(mSymbolTable, mNodeFactory, mErrorLog);
            mExecutionEnvironment = new InterpreterExecutionEnvironment(mErrorLog, mEventListener, new TestConsoleOutput());
        }

        /// <summary>
        /// Resets all parts of the interpreter.
        /// </summary>
        private void Reset()
        {
            mErrorLog = new ErrorLog();
            mSymbolTable = new SymbolTable(mNodeFactory);
            mTokenizer = new Tokenizer(mSymbolTable, mErrorLog);
            mParser = new Parser(mSymbolTable, mNodeFactory, mErrorLog);
            mExecutionEnvironment = new InterpreterExecutionEnvironment(mErrorLog, mEventListener, new TestConsoleOutput());
        }

        /// <summary>
        /// Get the data from a global variable.
        /// </summary>
        /// <param name="variableName">The variable's name.</param>
        /// <param name="environment">The execution environment.</param>
        /// <returns>The data from the variable.</returns>
        private Data GetGlobalVariableData(string variableName, InterpreterExecutionEnvironment environment)
        {
            return environment.GetDataSlot(variableName).Data;
        }

        /// <summary>
        /// Execute a given program.
        /// </summary>
        /// <param name="lines">The lines of the program to execute.</param>
        /// <returns>The execution environment of the program.</returns>
        private InterpreterExecutionEnvironment ExecuteProgram(params string[] lines)
        {
            // Build the source for the program
            var stringBuilder = new StringBuilder();
            foreach (var line in lines)
            {
                stringBuilder.AppendLine(line);
            }
            var source = stringBuilder.ToString();

            // Tokenize
            TokenStream stream;
            Assert.IsTrue(mTokenizer.Tokenize(source, out stream));

            // Parse
            ParseNode programNode;
            Assert.IsTrue(mParser.Parse(stream, out programNode));

            // Execute the program
            mExecutionEnvironment.ExecuteProgram((InterpreterParseNode)programNode);

            // Return the execution environment
            return mExecutionEnvironment;
        }

        /// <summary>
        /// Execute a given program and assert no errors have occured.
        /// </summary>
        /// <param name="lines">The ,ines of the program to execute.</param>
        /// <returns>The execution environment of the program.</returns>
        private InterpreterExecutionEnvironment ExecuteProgramWithoutErrors(params string[] lines)
        {
            this.ExecuteProgram(lines);

            // Check for errors
            Assert.AreEqual(0, mErrorLog.Entries.Count);

            return mExecutionEnvironment;
        }

        /// <summary>
        /// Tests assigning variables in the global scope.
        /// </summary>
        [TestMethod]
        public void Assignment_Literals()
        {
            var environment = this.ExecuteProgramWithoutErrors("var a = 5");
            var data = this.GetGlobalVariableData("a", environment);
            this.AssertInteger(data, 5);

            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var a = \"Hello World\"");
            data = this.GetGlobalVariableData("a", environment);
            this.AssertString(data, "Hello World");

            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var a = 1.1");
            data = this.GetGlobalVariableData("a", environment);
            this.AssertFloat(data, 1.1f);

            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var a = true");
            data = this.GetGlobalVariableData("a", environment);
            this.AssertBoolean(data, true);

            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var a = null");
            data = this.GetGlobalVariableData("a", environment);
            this.AssertNull(data);
        }

        /// <summary>
        /// Tests addition assignment operators in the global scope.
        /// </summary>
        [TestMethod]
        public void AdditionAssignment()
        {
            #region Valid additions

            // Int + Int
            var environment = this.ExecuteProgramWithoutErrors("var a = 5",
                                                  "a += 5");
            var data = this.GetGlobalVariableData("a", environment);
            this.AssertInteger(data, 10);

            // Float + Float
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var a = 1.1",
                                              "a += 2.2");
            data = this.GetGlobalVariableData("a", environment);
            this.AssertFloat(data, 3.3f);

            // String + String
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var a = \"Hello \"",
                                              "a += \"World\"");
            data = this.GetGlobalVariableData("a", environment);
            this.AssertString(data, "Hello World");

            // Int + Float
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var a = 5",
                                                  "a += 5.5");
            data = this.GetGlobalVariableData("a", environment);
            this.AssertFloat(data, 10.5f);

            // Float + Int
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var a = 1.1",
                                              "a += 2");
            data = this.GetGlobalVariableData("a", environment);
            this.AssertFloat(data, 3.1f);

            // String + Int
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var a = \"Hello \"",
                                              "a += 5");
            data = this.GetGlobalVariableData("a", environment);
            this.AssertString(data, "Hello 5");

            // String + Float
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var a = \"Hello \"",
                                              "a += 5.5");
            data = this.GetGlobalVariableData("a", environment);
            this.AssertString(data, "Hello 5.5");

            // Int + String
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var a = 5",
                                              "a += \"Five\"");
            data = this.GetGlobalVariableData("a", environment);
            this.AssertString(data, "5Five");

            // Float + String
            this.Reset();
            environment = this.ExecuteProgram("var a = 5.5",
                                              "a += \"Five\"");
            data = this.GetGlobalVariableData("a", environment);
            this.AssertString(data, "5.5Five");

            #endregion

            #region Boolean

            // Int + Boolean
            this.Reset();
            environment = this.ExecuteProgram("var a = 1",
                                              "a += true");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Float + Boolean
            this.Reset();
            environment = this.ExecuteProgram("var a = 1.111",
                                              "a += true");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // String + Boolean
            this.Reset();
            environment = this.ExecuteProgram("var a = \"Hello World\"",
                                              "a += true");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Object + Boolean
            this.Reset();
            environment = this.ExecuteProgram("define Thing { }",
                                              "var a = new Thing()",
                                              "a += true");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Null + Boolean
            this.Reset();
            environment = this.ExecuteProgram("var a = null",
                                              "a += true");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Boolean + Boolean
            this.Reset();
            environment = this.ExecuteProgram("var a = false",
                                              "a += true");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Boolean + Int
            this.Reset();
            environment = this.ExecuteProgram("var a = false",
                                              "a += 1");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Boolean + Float
            this.Reset();
            environment = this.ExecuteProgram("var a = false",
                                              "a += 12.1212");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Boolean + String
            this.Reset();
            environment = this.ExecuteProgram("var a = false",
                                              "a += \"Hey\"");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Boolean + Object
            this.Reset();
            environment = this.ExecuteProgram("define Thing { }",
                                              "var a = false",
                                              "a += new Thing()");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Boolean + Null
            this.Reset();
            environment = this.ExecuteProgram("var a = true",
                                              "a += null");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            #endregion

            #region Object

            // Object + Int
            this.Reset();
            environment = this.ExecuteProgram("define Thing { }",
                                              "var a = new Thing()",
                                              "a += 1");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Object + Float
            this.Reset();
            environment = this.ExecuteProgram("define Thing { }",
                                              "var a = new Thing()",
                                              "a += 737.2748");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Object + String
            this.Reset();
            environment = this.ExecuteProgram("define Thing { }",
                                              "var a = new Thing()",
                                              "a += \"Hello World\"");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Object + Null
            this.Reset();
            environment = this.ExecuteProgram("define Thing { }",
                                              "var a = new Thing()",
                                              "a += null");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Int + Object
            this.Reset();
            environment = this.ExecuteProgram("define Thing { }",
                                              "var a = 1",
                                              "a += new Thing()");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Float + Object
            this.Reset();
            environment = this.ExecuteProgram("define Thing { }",
                                              "var a = 8763.237",
                                              "a += new Thing()");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Null + Object
            this.Reset();
            environment = this.ExecuteProgram("define Thing { }",
                                              "var a = null",
                                              "a += new Thing()");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Object + Object
            this.Reset();
            environment = this.ExecuteProgram("define Thing { }",
                                              "var a = new Thing()",
                                              "a += new Thing()");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            #endregion

            #region Null

            // Null + Int
            this.Reset();
            environment = this.ExecuteProgram("var a = null",
                                              "a += 1");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Null + Float
            this.Reset();
            environment = this.ExecuteProgram("var a = null",
                                              "a += 346.123");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Null + String
            this.Reset();
            environment = this.ExecuteProgram("var a = null",
                                              "a += \"Hello World\"");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Null + Object
            this.Reset();
            environment = this.ExecuteProgram("define Thing { }",
                                              "var a = null",
                                              "a += 1");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Int + Null
            this.Reset();
            environment = this.ExecuteProgram("var a = 1",
                                              "a += null");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Float + Null
            this.Reset();
            environment = this.ExecuteProgram("var a = 2345.567",
                                              "a += null");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // String + Null
            this.Reset();
            environment = this.ExecuteProgram("var a = \"Hello World\"",
                                              "a += null");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Null + Null
            this.Reset();
            environment = this.ExecuteProgram("var a = null",
                                              "a += null");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            #endregion
        }

        /// <summary>
        /// Tests subtraction assignment operators in the global scope.
        /// </summary>
        [TestMethod]
        public void SubtractionAssignment()
        {
            #region Valid

            // Int - Int
            var environment = this.ExecuteProgramWithoutErrors("var a = 5",
                                                  "a -= 5");
            var data = this.GetGlobalVariableData("a", environment);
            this.AssertInteger(data, 0);

            // Float - Float
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var a = 1.1",
                                              "a -= 2.2");
            data = this.GetGlobalVariableData("a", environment);
            this.AssertFloat(data, -1.1f);

            // Int - Float
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var a = 5",
                                                  "a -= 5.5");
            data = this.GetGlobalVariableData("a", environment);
            this.AssertFloat(data, -0.5f);

            // Float - Int
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var a = 1.1",
                                              "a -= 2");
            data = this.GetGlobalVariableData("a", environment);
            this.AssertFloat(data, -0.9f);

            #endregion

            #region Boolean

            // Int - Boolean
            this.Reset();
            environment = this.ExecuteProgram("var a = 1",
                                              "a -= true");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Float - Boolean
            this.Reset();
            environment = this.ExecuteProgram("var a = 1.111",
                                              "a -= true");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // String - Boolean
            this.Reset();
            environment = this.ExecuteProgram("var a = \"Hello World\"",
                                              "a -= true");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Object - Boolean
            this.Reset();
            environment = this.ExecuteProgram("define Thing { }",
                                              "var a = new Thing()",
                                              "a -= true");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Null - Boolean
            this.Reset();
            environment = this.ExecuteProgram("var a = null",
                                              "a -= true");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Boolean - Boolean
            this.Reset();
            environment = this.ExecuteProgram("var a = false",
                                              "a -= true");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Boolean - Int
            this.Reset();
            environment = this.ExecuteProgram("var a = false",
                                              "a -= 1");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Boolean - Float
            this.Reset();
            environment = this.ExecuteProgram("var a = false",
                                              "a -= 12.1212");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Boolean - String
            this.Reset();
            environment = this.ExecuteProgram("var a = false",
                                              "a -= \"Hey\"");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Boolean - Object
            this.Reset();
            environment = this.ExecuteProgram("define Thing { }",
                                              "var a = false",
                                              "a -= new Thing()");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Boolean - Null
            this.Reset();
            environment = this.ExecuteProgram("var a = true",
                                              "a -= null");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            #endregion

            #region Object

            // Object - Int
            this.Reset();
            environment = this.ExecuteProgram("define Thing { }",
                                              "var a = new Thing()",
                                              "a -= 1");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Object - Float
            this.Reset();
            environment = this.ExecuteProgram("define Thing { }",
                                              "var a = new Thing()",
                                              "a -= 737.2748");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Object - String
            this.Reset();
            environment = this.ExecuteProgram("define Thing { }",
                                              "var a = new Thing()",
                                              "a -= \"Hello World\"");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Object - Null
            this.Reset();
            environment = this.ExecuteProgram("define Thing { }",
                                              "var a = new Thing()",
                                              "a -= null");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Int - Object
            this.Reset();
            environment = this.ExecuteProgram("define Thing { }",
                                              "var a = 1",
                                              "a -= new Thing()");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Float - Object
            this.Reset();
            environment = this.ExecuteProgram("define Thing { }",
                                              "var a = 8763.237",
                                              "a -= new Thing()");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Null - Object
            this.Reset();
            environment = this.ExecuteProgram("define Thing { }",
                                              "var a = null",
                                              "a -= new Thing()");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Object - Object
            this.Reset();
            environment = this.ExecuteProgram("define Thing { }",
                                              "var a = new Thing()",
                                              "a -= new Thing()");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            #endregion

            #region Null

            // Null - Int
            this.Reset();
            environment = this.ExecuteProgram("var a = null",
                                              "a -= 1");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Null - Float
            this.Reset();
            environment = this.ExecuteProgram("var a = null",
                                              "a -= 346.123");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Null - String
            this.Reset();
            environment = this.ExecuteProgram("var a = null",
                                              "a -= \"Hello World\"");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Null - Object
            this.Reset();
            environment = this.ExecuteProgram("define Thing { }",
                                              "var a = null",
                                              "a -= 1");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Int - Null
            this.Reset();
            environment = this.ExecuteProgram("var a = 1",
                                              "a -= null");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Float - Null
            this.Reset();
            environment = this.ExecuteProgram("var a = 2345.567",
                                              "a -= null");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // String - Null
            this.Reset();
            environment = this.ExecuteProgram("var a = \"Hello World\"",
                                              "a -= null");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Null - Null
            this.Reset();
            environment = this.ExecuteProgram("var a = null",
                                              "a -= null");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            #endregion

            #region String

            // String - Int
            this.Reset();
            environment = this.ExecuteProgram("var a = \"Hello World\"",
                                              "a -= 1");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // String - Float
            this.Reset();
            environment = this.ExecuteProgram("var a = \"Hello World\"",
                                              "a -= 234.5678");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // String - Boolean
            this.Reset();
            environment = this.ExecuteProgram("var a = \"Hello World\"",
                                              "a -= true");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // String - Object
            this.Reset();
            environment = this.ExecuteProgram("define Thing { }",
                                              "var a = \"Hello World\"",
                                              "a -= new Thing()");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // String - Null
            this.Reset();
            environment = this.ExecuteProgram("var a = \"Hello World\"",
                                              "a -= null");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // String - String
            this.Reset();
            environment = this.ExecuteProgram("var a = \"Hello World\"",
                                              "a -= \"Help Help\"");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            #endregion
        }

        /// <summary>
        /// Tests modulo assignment operators in the global scope.
        /// </summary>
        [TestMethod]
        public void ModuloAssignment()
        {
            #region Valid

            // Int % Int
            var environment = this.ExecuteProgramWithoutErrors("var a = 10",
                                                  "a %= 2");
            var data = this.GetGlobalVariableData("a", environment);
            this.AssertInteger(data, 0);

            // Float % Float
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var a = 12.12",
                                              "a %= 5.5");
            data = this.GetGlobalVariableData("a", environment);
            this.AssertFloat(data, 1.12f);

            // Int % Float
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var a = 8",
                                              "a %= 5.5");
            data = this.GetGlobalVariableData("a", environment);
            this.AssertFloat(data, 2.5f);

            // Float % Int
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var a = 23.12",
                                              "a %= 3");
            data = this.GetGlobalVariableData("a", environment);
            this.AssertFloat(data, 2.12f);

            #endregion

            #region Boolean

            // Int % Boolean
            this.Reset();
            environment = this.ExecuteProgram("var a = 1",
                                              "a %= true");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Float % Boolean
            this.Reset();
            environment = this.ExecuteProgram("var a = 1.111",
                                              "a %= true");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // String % Boolean
            this.Reset();
            environment = this.ExecuteProgram("var a = \"Hello World\"",
                                              "a %= true");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Object % Boolean
            this.Reset();
            environment = this.ExecuteProgram("define Thing { }",
                                              "var a = new Thing()",
                                              "a %= true");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Null % Boolean
            this.Reset();
            environment = this.ExecuteProgram("var a = null",
                                              "a %= true");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Boolean % Boolean
            this.Reset();
            environment = this.ExecuteProgram("var a = false",
                                              "a %= true");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Boolean % Int
            this.Reset();
            environment = this.ExecuteProgram("var a = false",
                                              "a %= 1");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Boolean % Float
            this.Reset();
            environment = this.ExecuteProgram("var a = false",
                                              "a %= 12.1212");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Boolean % String
            this.Reset();
            environment = this.ExecuteProgram("var a = false",
                                              "a %= \"Hey\"");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Boolean % Object
            this.Reset();
            environment = this.ExecuteProgram("define Thing { }",
                                              "var a = false",
                                              "a %= new Thing()");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Boolean % Null
            this.Reset();
            environment = this.ExecuteProgram("var a = true",
                                              "a %= null");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            #endregion

            #region Object

            // Object % Int
            this.Reset();
            environment = this.ExecuteProgram("define Thing { }",
                                              "var a = new Thing()",
                                              "a %= 1");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Object % Float
            this.Reset();
            environment = this.ExecuteProgram("define Thing { }",
                                              "var a = new Thing()",
                                              "a %= 737.2748");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Object % String
            this.Reset();
            environment = this.ExecuteProgram("define Thing { }",
                                              "var a = new Thing()",
                                              "a %= \"Hello World\"");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Object % Null
            this.Reset();
            environment = this.ExecuteProgram("define Thing { }",
                                              "var a = new Thing()",
                                              "a %= null");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Int % Object
            this.Reset();
            environment = this.ExecuteProgram("define Thing { }",
                                              "var a = 1",
                                              "a %= new Thing()");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Float % Object
            this.Reset();
            environment = this.ExecuteProgram("define Thing { }",
                                              "var a = 8763.237",
                                              "a %= new Thing()");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Null % Object
            this.Reset();
            environment = this.ExecuteProgram("define Thing { }",
                                              "var a = null",
                                              "a %= new Thing()");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Object % Object
            this.Reset();
            environment = this.ExecuteProgram("define Thing { }",
                                              "var a = new Thing()",
                                              "a %= new Thing()");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            #endregion

            #region Null

            // Null % Int
            this.Reset();
            environment = this.ExecuteProgram("var a = null",
                                              "a %= 1");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Null % Float
            this.Reset();
            environment = this.ExecuteProgram("var a = null",
                                              "a %= 346.123");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Null % String
            this.Reset();
            environment = this.ExecuteProgram("var a = null",
                                              "a %= \"Hello World\"");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Null % Object
            this.Reset();
            environment = this.ExecuteProgram("define Thing { }",
                                              "var a = null",
                                              "a %= 1");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Int % Null
            this.Reset();
            environment = this.ExecuteProgram("var a = 1",
                                              "a %= null");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Float % Null
            this.Reset();
            environment = this.ExecuteProgram("var a = 2345.567",
                                              "a %= null");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // String % Null
            this.Reset();
            environment = this.ExecuteProgram("var a = \"Hello World\"",
                                              "a %= null");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Null % Null
            this.Reset();
            environment = this.ExecuteProgram("var a = null",
                                              "a %= null");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            #endregion

            #region String

            // String % Int
            this.Reset();
            environment = this.ExecuteProgram("var a = \"Hello World\"",
                                              "a %= 1");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // String % Float
            this.Reset();
            environment = this.ExecuteProgram("var a = \"Hello World\"",
                                              "a %= 234.5678");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // String % Boolean
            this.Reset();
            environment = this.ExecuteProgram("var a = \"Hello World\"",
                                              "a %= true");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // String % Object
            this.Reset();
            environment = this.ExecuteProgram("define Thing { }",
                                              "var a = \"Hello World\"",
                                              "a %= new Thing()");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // String % Null
            this.Reset();
            environment = this.ExecuteProgram("var a = \"Hello World\"",
                                              "a %= null");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // String % String
            this.Reset();
            environment = this.ExecuteProgram("var a = \"Hello World\"",
                                              "a %= \"Help Help\"");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            #endregion
        }

        /// <summary>
        /// Tests division assignment operators in the global scope.
        /// </summary>
        [TestMethod]
        public void DivisionAssignment()
        {
            #region Valid

            // Int / Int
            var environment = this.ExecuteProgramWithoutErrors("var a = 10",
                                                  "a /= 2");
            var data = this.GetGlobalVariableData("a", environment);
            this.AssertInteger(data, 5);

            // Float / Float
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var a = 12.12",
                                              "a /= 5.5");
            data = this.GetGlobalVariableData("a", environment);
            this.AssertFloat(data, 2.20363636f);

            // Int / Float
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var a = 8",
                                              "a /= 5.5");
            data = this.GetGlobalVariableData("a", environment);
            this.AssertFloat(data, 1.45454545f);

            // Float / Int
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var a = 23.12",
                                              "a /= 3");
            data = this.GetGlobalVariableData("a", environment);
            this.AssertFloat(data, 7.70666666667f);

            #endregion

            #region Boolean

            // Int / Boolean
            this.Reset();
            environment = this.ExecuteProgram("var a = 1",
                                              "a /= true");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Float / Boolean
            this.Reset();
            environment = this.ExecuteProgram("var a = 1.111",
                                              "a /= true");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // String / Boolean
            this.Reset();
            environment = this.ExecuteProgram("var a = \"Hello World\"",
                                              "a /= true");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Object / Boolean
            this.Reset();
            environment = this.ExecuteProgram("define Thing { }",
                                              "var a = new Thing()",
                                              "a /= true");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Null / Boolean
            this.Reset();
            environment = this.ExecuteProgram("var a = null",
                                              "a /= true");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Boolean / Boolean
            this.Reset();
            environment = this.ExecuteProgram("var a = false",
                                              "a /= true");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Boolean / Int
            this.Reset();
            environment = this.ExecuteProgram("var a = false",
                                              "a /= 1");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Boolean / Float
            this.Reset();
            environment = this.ExecuteProgram("var a = false",
                                              "a /= 12.1212");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Boolean / String
            this.Reset();
            environment = this.ExecuteProgram("var a = false",
                                              "a /= \"Hey\"");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Boolean / Object
            this.Reset();
            environment = this.ExecuteProgram("define Thing { }",
                                              "var a = false",
                                              "a /= new Thing()");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Boolean / Null
            this.Reset();
            environment = this.ExecuteProgram("var a = true",
                                              "a /= null");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            #endregion

            #region Object

            // Object / Int
            this.Reset();
            environment = this.ExecuteProgram("define Thing { }",
                                              "var a = new Thing()",
                                              "a /= 1");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Object / Float
            this.Reset();
            environment = this.ExecuteProgram("define Thing { }",
                                              "var a = new Thing()",
                                              "a /= 737.2748");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Object / String
            this.Reset();
            environment = this.ExecuteProgram("define Thing { }",
                                              "var a = new Thing()",
                                              "a /= \"Hello World\"");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Object / Null
            this.Reset();
            environment = this.ExecuteProgram("define Thing { }",
                                              "var a = new Thing()",
                                              "a /= null");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Int / Object
            this.Reset();
            environment = this.ExecuteProgram("define Thing { }",
                                              "var a = 1",
                                              "a /= new Thing()");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Float / Object
            this.Reset();
            environment = this.ExecuteProgram("define Thing { }",
                                              "var a = 8763.237",
                                              "a /= new Thing()");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Null / Object
            this.Reset();
            environment = this.ExecuteProgram("define Thing { }",
                                              "var a = null",
                                              "a /= new Thing()");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Object / Object
            this.Reset();
            environment = this.ExecuteProgram("define Thing { }",
                                              "var a = new Thing()",
                                              "a /= new Thing()");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            #endregion

            #region Null

            // Null / Int
            this.Reset();
            environment = this.ExecuteProgram("var a = null",
                                              "a /= 1");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Null / Float
            this.Reset();
            environment = this.ExecuteProgram("var a = null",
                                              "a /= 346.123");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Null / String
            this.Reset();
            environment = this.ExecuteProgram("var a = null",
                                              "a /= \"Hello World\"");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Null / Object
            this.Reset();
            environment = this.ExecuteProgram("define Thing { }",
                                              "var a = null",
                                              "a /= 1");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Int / Null
            this.Reset();
            environment = this.ExecuteProgram("var a = 1",
                                              "a /= null");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Float / Null
            this.Reset();
            environment = this.ExecuteProgram("var a = 2345.567",
                                              "a /= null");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // String / Null
            this.Reset();
            environment = this.ExecuteProgram("var a = \"Hello World\"",
                                              "a /= null");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Null / Null
            this.Reset();
            environment = this.ExecuteProgram("var a = null",
                                              "a /= null");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            #endregion

            #region String

            // String / Int
            this.Reset();
            environment = this.ExecuteProgram("var a = \"Hello World\"",
                                              "a /= 1");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // String / Float
            this.Reset();
            environment = this.ExecuteProgram("var a = \"Hello World\"",
                                              "a /= 234.5678");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // String / Boolean
            this.Reset();
            environment = this.ExecuteProgram("var a = \"Hello World\"",
                                              "a /= true");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // String / Object
            this.Reset();
            environment = this.ExecuteProgram("define Thing { }",
                                              "var a = \"Hello World\"",
                                              "a /= new Thing()");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // String / Null
            this.Reset();
            environment = this.ExecuteProgram("var a = \"Hello World\"",
                                              "a /= null");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // String / String
            this.Reset();
            environment = this.ExecuteProgram("var a = \"Hello World\"",
                                              "a /= \"Help Help\"");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            #endregion
        }

        /// <summary>
        /// Tests multiplication assignment operators in the global scope.
        /// </summary>
        [TestMethod]
        public void MultiplicationAssignment()
        {
            #region Valid

            // Int * Int
            var environment = this.ExecuteProgramWithoutErrors("var a = 10",
                                                  "a *= 2");
            var data = this.GetGlobalVariableData("a", environment);
            this.AssertInteger(data, 20);

            // Float * Float
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var a = 12.12",
                                              "a *= 5.5");
            data = this.GetGlobalVariableData("a", environment);
            this.AssertFloat(data, 12.12f * 5.5f);

            // Int * Float
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var a = 8",
                                              "a *= 5.5");
            data = this.GetGlobalVariableData("a", environment);
            this.AssertFloat(data, 44.0f);

            // Float * Int
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var a = 23.12",
                                              "a *= 3");
            data = this.GetGlobalVariableData("a", environment);
            this.AssertFloat(data, 69.36f);

            #endregion

            #region Boolean

            // Int * Boolean
            this.Reset();
            environment = this.ExecuteProgram("var a = 1",
                                              "a /= true");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Float * Boolean
            this.Reset();
            environment = this.ExecuteProgram("var a = 1.111",
                                              "a /= true");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // String * Boolean
            this.Reset();
            environment = this.ExecuteProgram("var a = \"Hello World\"",
                                              "a /= true");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Object * Boolean
            this.Reset();
            environment = this.ExecuteProgram("define Thing { }",
                                              "var a = new Thing()",
                                              "a /= true");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Null * Boolean
            this.Reset();
            environment = this.ExecuteProgram("var a = null",
                                              "a /= true");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Boolean * Boolean
            this.Reset();
            environment = this.ExecuteProgram("var a = false",
                                              "a /= true");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Boolean * Int
            this.Reset();
            environment = this.ExecuteProgram("var a = false",
                                              "a /= 1");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Boolean * Float
            this.Reset();
            environment = this.ExecuteProgram("var a = false",
                                              "a /= 12.1212");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Boolean * String
            this.Reset();
            environment = this.ExecuteProgram("var a = false",
                                              "a /= \"Hey\"");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Boolean * Object
            this.Reset();
            environment = this.ExecuteProgram("define Thing { }",
                                              "var a = false",
                                              "a /= new Thing()");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Boolean * Null
            this.Reset();
            environment = this.ExecuteProgram("var a = true",
                                              "a /= null");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            #endregion

            #region Object

            // Object * Int
            this.Reset();
            environment = this.ExecuteProgram("define Thing { }",
                                              "var a = new Thing()",
                                              "a /= 1");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Object * Float
            this.Reset();
            environment = this.ExecuteProgram("define Thing { }",
                                              "var a = new Thing()",
                                              "a /= 737.2748");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Object * String
            this.Reset();
            environment = this.ExecuteProgram("define Thing { }",
                                              "var a = new Thing()",
                                              "a /= \"Hello World\"");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Object * Null
            this.Reset();
            environment = this.ExecuteProgram("define Thing { }",
                                              "var a = new Thing()",
                                              "a /= null");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Int * Object
            this.Reset();
            environment = this.ExecuteProgram("define Thing { }",
                                              "var a = 1",
                                              "a /= new Thing()");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Float * Object
            this.Reset();
            environment = this.ExecuteProgram("define Thing { }",
                                              "var a = 8763.237",
                                              "a /= new Thing()");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Null * Object
            this.Reset();
            environment = this.ExecuteProgram("define Thing { }",
                                              "var a = null",
                                              "a /= new Thing()");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Object * Object
            this.Reset();
            environment = this.ExecuteProgram("define Thing { }",
                                              "var a = new Thing()",
                                              "a /= new Thing()");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            #endregion

            #region Null

            // Null * Int
            this.Reset();
            environment = this.ExecuteProgram("var a = null",
                                              "a /= 1");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Null * Float
            this.Reset();
            environment = this.ExecuteProgram("var a = null",
                                              "a /= 346.123");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Null * String
            this.Reset();
            environment = this.ExecuteProgram("var a = null",
                                              "a /= \"Hello World\"");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Null * Object
            this.Reset();
            environment = this.ExecuteProgram("define Thing { }",
                                              "var a = null",
                                              "a /= 1");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Int * Null
            this.Reset();
            environment = this.ExecuteProgram("var a = 1",
                                              "a /= null");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Float * Null
            this.Reset();
            environment = this.ExecuteProgram("var a = 2345.567",
                                              "a /= null");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // String * Null
            this.Reset();
            environment = this.ExecuteProgram("var a = \"Hello World\"",
                                              "a /= null");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // Null * Null
            this.Reset();
            environment = this.ExecuteProgram("var a = null",
                                              "a /= null");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            #endregion

            #region String

            // String * Int
            this.Reset();
            environment = this.ExecuteProgram("var a = \"Hello World\"",
                                              "a /= 1");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // String * Float
            this.Reset();
            environment = this.ExecuteProgram("var a = \"Hello World\"",
                                              "a /= 234.5678");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // String * Boolean
            this.Reset();
            environment = this.ExecuteProgram("var a = \"Hello World\"",
                                              "a /= true");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // String * Object
            this.Reset();
            environment = this.ExecuteProgram("define Thing { }",
                                              "var a = \"Hello World\"",
                                              "a /= new Thing()");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // String * Null
            this.Reset();
            environment = this.ExecuteProgram("var a = \"Hello World\"",
                                              "a /= null");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            // String * String
            this.Reset();
            environment = this.ExecuteProgram("var a = \"Hello World\"",
                                              "a /= \"Help Help\"");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            #endregion
        }

        /// <summary>
        /// Tests if statements in the global scope.
        /// </summary>
        [TestMethod]
        public void IfStatement()
        {
            // Test single if
            var environment = this.ExecuteProgramWithoutErrors("var a = true",
                                                  "var test = 0",
                                                  "if (a)",
                                                  "{",
                                                  "test = 5",
                                                  "}");
            var data = this.GetGlobalVariableData("test", environment);
            this.AssertInteger(data, 5);

            // Test if else
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var a = false",
                                              "var test1 = 1",
                                              "var test2 = 2",
                                              "if (a)",
                                              "{",
                                              "test1 = 5",
                                              "}",
                                              "else",
                                              "{",
                                              "test2 = 10",
                                              "}");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertInteger(data, 1);
            data = this.GetGlobalVariableData("test2", environment);
            this.AssertInteger(data, 10);

            // Test if else if else
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var a = false",
                                              "var test1 = 1",
                                              "var test2 = 2",
                                              "var test3 = 3",
                                              "if (a)",
                                              "{",
                                                "test1 = 5",
                                              "}",
                                              "else if (true)",
                                              "{",
                                                "test2 = 10",
                                              "}",
                                              "else",
                                              "{",
                                                "test3 = 15",
                                              "}");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertInteger(data, 1);
            data = this.GetGlobalVariableData("test2", environment);
            this.AssertInteger(data, 10);
            data = this.GetGlobalVariableData("test3", environment);
            this.AssertInteger(data, 3);

            // Test if else if else
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var a = false",
                                              "var test1 = 1",
                                              "var test2 = 2",
                                              "var test3 = 3",
                                              "if (a)",
                                              "{",
                                              "test1 = 5",
                                              "}",
                                              "else if (false)",
                                              "{",
                                              "test2 = 10",
                                              "}",
                                              "else",
                                              "{",
                                              "test3 = 15",
                                              "}");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertInteger(data, 1);
            data = this.GetGlobalVariableData("test2", environment);
            this.AssertInteger(data, 2);
            data = this.GetGlobalVariableData("test3", environment);
            this.AssertInteger(data, 15);

            // Test without parenthesis
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var a = false",
                                              "var test1 = 1",
                                              "var test2 = 2",
                                              "var test3 = 3",
                                              "if a",
                                              "{",
                                              "test1 = 5",
                                              "}",
                                              "else if false",
                                              "{",
                                              "test2 = 10",
                                              "}",
                                              "else",
                                              "{",
                                              "test3 = 15",
                                              "}");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertInteger(data, 1);
            data = this.GetGlobalVariableData("test2", environment);
            this.AssertInteger(data, 2);
            data = this.GetGlobalVariableData("test3", environment);
            this.AssertInteger(data, 15);

            // Test nested
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test",
                                              "if true",
                                              "{",
                                              "if true",
                                              "{",
                                              "test = 15",
                                              "}",
                                              "}");
            data = this.GetGlobalVariableData("test", environment);
            this.AssertInteger(data, 15);

            // Test stack frame
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("if (true)",
                                              "{",
                                              "var thing = 5",
                                              "}",
                                              "var thing = 2");
            data = this.GetGlobalVariableData("thing", environment);
            this.AssertInteger(data, 2);

            // Test one line
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test",
                                              "if (true) { test = 5; } else { test = 1; }");
            data = this.GetGlobalVariableData("test", environment);
            this.AssertInteger(data, 5);

            // Test one line without parentheses
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test",
                                              "if true { test = 5; } else { test = 1; }");
            data = this.GetGlobalVariableData("test", environment);
            this.AssertInteger(data, 5);

            // Test one "if else if"
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test",
                                              "if (false) { test = 5; } else if (true) { test = 1; }");
            data = this.GetGlobalVariableData("test", environment);
            this.AssertInteger(data, 1);

            // Test one "if else if" without parenthesis
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test",
                                              "if false { test = 5; } else if true { test = 1; }");
            data = this.GetGlobalVariableData("test", environment);
            this.AssertInteger(data, 1);
        }

        /// <summary>
        /// Tests the for statement.
        /// </summary>
        [TestMethod]
        public void ForLoop()
        {
            // Test standard usage
            var environment = this.ExecuteProgramWithoutErrors("var test = 0",
                                                  "for (var i = 0; i < 5; i += 1)",
                                                  "{",
                                                  "test += 2",
                                                  "}");
            var data = this.GetGlobalVariableData("test", environment);
            this.AssertInteger(data, 10);

            // Test creating variables inside the loop
            this.Reset();
            environment = this.ExecuteProgram("var test = 0",
                                                "for (var i = 0; i < 5; i += 1)",
                                                "{",
                                                "var a = 5",
                                                "test += 2",
                                                "}");
            data = this.GetGlobalVariableData("test", environment);
            this.AssertInteger(data, 10);

            // Test without parenthesis usage
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test = 0",
                                              "for var i = 0; i < 5; i += 1",
                                              "{",
                                              "test += 2",
                                              "}");
            data = this.GetGlobalVariableData("test", environment);
            this.AssertInteger(data, 10);

            // Test on different lines
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test = 0",
                                              "for (var i = 0",
                                              " i < 5",
                                              " i += 1)",
                                              "{",
                                              "test += 2",
                                              "}");
            data = this.GetGlobalVariableData("test", environment);
            this.AssertInteger(data, 10);

            // Test without declaration
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test = 0",
                                              "var i = 0",
                                              "for (; i < 5; i += 1)",
                                              "{",
                                              "test += 2",
                                              "}");
            data = this.GetGlobalVariableData("test", environment);
            this.AssertInteger(data, 10);

            // Test without declaration or parenthesis
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test = 0",
                                              "var i = 0",
                                              "for ; i < 5; i += 1",
                                              "{",
                                              "test += 2",
                                              "}");
            data = this.GetGlobalVariableData("test", environment);
            this.AssertInteger(data, 10);

            // Test without assignment
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test = 0",
                                              "for (var i = 0; i < 5;)",
                                              "{",
                                              "test += 2",
                                              "i += 1",
                                              "}");
            data = this.GetGlobalVariableData("test", environment);
            this.AssertInteger(data, 10);

            // Test without assignment or parenthesis
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test = 0",
                                              "for var i = 0; i < 5;",
                                              "{",
                                              "test += 2",
                                              "i += 1",
                                              "}");
            data = this.GetGlobalVariableData("test", environment);
            this.AssertInteger(data, 10);


            // Test without comparison
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test = 0",
                                              "for (var i = 0; ; i += 1)",
                                              "{",
                                              "test += 2",
                                              "break",
                                              "}");
            data = this.GetGlobalVariableData("test", environment);
            this.AssertInteger(data, 2);

            // Test assignment breaking
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test = 0",
                                              "var i = 0",
                                              "for (; ; i += 1)",
                                              "{",
                                              "test += 2",
                                              "break",
                                              "}");
            data = this.GetGlobalVariableData("test", environment);
            this.AssertInteger(data, 2);
            data = this.GetGlobalVariableData("i", environment);
            this.AssertInteger(data, 0);

            // Test nested
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test = 0",
                                              "for (var i = 0; i < 10; i += 1)",
                                              "{",
                                              "for (var j = 0; j < 10; j += 1)",
                                              "{",
                                              "test += 1",
                                              "}",
                                              "}");
            data = this.GetGlobalVariableData("test", environment);
            this.AssertInteger(data, 100);

            // Test one line
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test = 0",
                                              "for (var i = 0; i < 10; i += 1) { test += 1; }");
            data = this.GetGlobalVariableData("test", environment);
            this.AssertInteger(data, 10);

            // Test one line without parenthesis
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test = 0",
                                              "for var i = 0; i < 10; i += 1 { test += 1; }");
            data = this.GetGlobalVariableData("test", environment);
            this.AssertInteger(data, 10);
        }

        /// <summary>
        /// Tests that while loops execute correctly.
        /// </summary>
        [TestMethod]
        public void WhileLoop()
        {
            // Test standard usage
            var environment = this.ExecuteProgramWithoutErrors("var test = 0",
                                                  "while (test < 5)",
                                                  "{",
                                                  "test += 1",
                                                  "}");
            var data = this.GetGlobalVariableData("test", environment);
            this.AssertInteger(data, 5);

            // Test without parentheses
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test = 0",
                                              "while test < 5",
                                              "{",
                                              "test += 1",
                                              "}");
            data = this.GetGlobalVariableData("test", environment);
            this.AssertInteger(data, 5);

            // Test nested
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 0",
                                              "var test2 = 0",
                                              "while test1 < 5",
                                              "{",
                                              "test1 += 1",
                                              "test2 = 0",
                                              "while test2 < 10",
                                              "{",
                                              "test2 += 1",
                                              "}",
                                              "}");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertInteger(data, 5);
            data = this.GetGlobalVariableData("test2", environment);
            this.AssertInteger(data, 10);

            // Test one line
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test = 0",
                                              "while (test < 5) { test += 1; }");
            data = this.GetGlobalVariableData("test", environment);
            this.AssertInteger(data, 5);

            // Test one line without parenthesis
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test = 0",
                                              "while test < 5 { test += 1; }");
            data = this.GetGlobalVariableData("test", environment);
            this.AssertInteger(data, 5);
        }

        /// <summary>
        /// Tests the increment construct.
        /// </summary>
        [TestMethod]
        public void PostfixIncrement()
        {
            // Test integer
            var environment = this.ExecuteProgramWithoutErrors("var test1 = 0",
                                                  "var test2 = test1++");
            var data = this.GetGlobalVariableData("test1", environment);
            this.AssertInteger(data, 1);
            data = this.GetGlobalVariableData("test2", environment);
            this.AssertInteger(data, 0);

            // Test integer assignment statement
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 0",
                                              "test1++");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertInteger(data, 1);

            // Test float
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 1.73",
                                              "var test2 = test1++");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertFloat(data, 2.73f);
            data = this.GetGlobalVariableData("test2", environment);
            this.AssertFloat(data, 1.73f);

            // Test float assignment statement
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 1.73",
                                              "test1++");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertFloat(data, 2.73f);
        }

        /// <summary>
        /// Tests the increment construct.
        /// </summary>
        [TestMethod]
        public void PrefixIncrement()
        {
            // Test integer
            var environment = this.ExecuteProgramWithoutErrors("var test1 = 0",
                                                  "var test2 = ++test1");
            var data = this.GetGlobalVariableData("test1", environment);
            this.AssertInteger(data, 1);
            data = this.GetGlobalVariableData("test2", environment);
            this.AssertInteger(data, 1);

            // Test integer assignment statement
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 0",
                                              "++test1");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertInteger(data, 1);

            // Test float
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 1.73",
                                              "var test2 = ++test1");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertFloat(data, 2.73f);
            data = this.GetGlobalVariableData("test2", environment);
            this.AssertFloat(data, 2.73f);

            // Test float assignment statement
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 1.73",
                                              "++test1");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertFloat(data, 2.73f);

            // Test variable does not become referenced
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 0; var test2 = ++test1; ++test1");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertInteger(data, 2);
            data = this.GetGlobalVariableData("test2", environment);
            this.AssertInteger(data, 1);
        }

        /// <summary>
        /// Tests the decrement construct.
        /// </summary>
        [TestMethod]
        public void PostfixDecrement()
        {
            // Test integer
            var environment = this.ExecuteProgramWithoutErrors("var test1 = 0",
                                                  "var test2 = test1--");
            var data = this.GetGlobalVariableData("test1", environment);
            this.AssertInteger(data, -1);
            data = this.GetGlobalVariableData("test2", environment);
            this.AssertInteger(data, 0);

            // Test integer assignment statement
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 0",
                                              "test1--");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertInteger(data, -1);

            // Test float
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 1.73",
                                              "var test2 = test1--");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertFloat(data, 0.73f);
            data = this.GetGlobalVariableData("test2", environment);
            this.AssertFloat(data, 1.73f);

            // Test float assignment statement
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 1.73",
                                              "test1--");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertFloat(data, 0.73f);
        }

        /// <summary>
        /// Tests the decrement construct.
        /// </summary>
        [TestMethod]
        public void PrefixDecrement()
        {
            // Test integer
            var environment = this.ExecuteProgramWithoutErrors("var test1 = 0",
                                                  "var test2 = --test1");
            var data = this.GetGlobalVariableData("test1", environment);
            this.AssertInteger(data, -1);
            data = this.GetGlobalVariableData("test2", environment);
            this.AssertInteger(data, -1);

            // Test integer assignment statement
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 0",
                                              "--test1");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertInteger(data, -1);

            // Test float
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 1.73",
                                              "var test2 = --test1");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertFloat(data, 0.73f);
            data = this.GetGlobalVariableData("test2", environment);
            this.AssertFloat(data, 0.73f);

            // Test float assignment statement
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 1.73",
                                              "--test1");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertFloat(data, 0.73f);

            // Test variable does not become referenced
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 0; var test2 = --test1; --test1");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertInteger(data, -2);
            data = this.GetGlobalVariableData("test2", environment);
            this.AssertInteger(data, -1);
        }

        /// <summary>
        /// Tests integer arithmetic.
        /// </summary>
        [TestMethod]
        public void IntegerArithmetic()
        {
            // Test 1
            var environment = this.ExecuteProgramWithoutErrors("var test = 1 + 2 + 3 + 4 + 5 + 6 + 7 + -8 + 9");
            var data = this.GetGlobalVariableData("test", environment);
            this.AssertInteger(data, 1 + 2 + 3 + 4 + 5 + 6 + 7 + -8 + 9);

            // Test 2
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test = 1 + 2 - 3 * -4 + 5 + 6 - 7 * 8 + 9");
            data = this.GetGlobalVariableData("test", environment);
            this.AssertInteger(data, 1 + 2 - 3 * -4 + 5 + 6 - 7 * 8 + 9);

            // Test 3
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test = 12 + 54 / 2");
            data = this.GetGlobalVariableData("test", environment);
            this.AssertInteger(data, 12 + 54 / 2);

            // Test 3
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test = 5 / 2");
            data = this.GetGlobalVariableData("test", environment);
            this.AssertInteger(data, 5 / 2);

            // Test 3
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test = 4 / 3 + 12 * 1 / 3");
            data = this.GetGlobalVariableData("test", environment);
            this.AssertInteger(data, 4 / 3 + 12 * 1 / 3);

            // Test 4
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test = 234 + (12 - -123 * 2) / (123 * (1 + 2 + 3 / 12))");
            data = this.GetGlobalVariableData("test", environment);
            this.AssertInteger(data, 234 + (12 - -123 * 2) / (123 * (1 + 2 + 3 / 12)));

            // Test 5
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test = 23 % 3 + 12 % -8");
            data = this.GetGlobalVariableData("test", environment);
            this.AssertInteger(data, 23 % 3 + 12 % -8);

            // Test 6
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test = 234 + (12 - 123 * 2) % (123 * (1 + 2 + 3 / 12)) % 12 + 54 / 2");
            data = this.GetGlobalVariableData("test", environment);
            this.AssertInteger(data, 234 + (12 - 123 * 2) % (123 * (1 + 2 + 3 / 12)) % 12 + 54 / 2);
        }

        /// <summary>
        /// Tests float arithmetic.
        /// </summary>
        [TestMethod]
        public void FloatArithmetic()
        {
            // Test 1
            var environment = this.ExecuteProgramWithoutErrors("var test = 1.41 + 2.78 + 3.12 + 4.76 + 5.123 + 6.751 + 7.72 + -8.12 + 9.61");
            var data = this.GetGlobalVariableData("test", environment);
            this.AssertFloat(data, 1.41f + 2.78f + 3.12f + 4.76f + 5.123f + 6.751f + 7.72f + -8.12f + 9.61f);

            // Test 2
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test = 1.62 + 2.3214 - 3.14 * -4.7 + 5.1 + 6.75 - 7.32 * 8.61 + 9.19");
            data = this.GetGlobalVariableData("test", environment);
            this.AssertFloat(data, 1.62f + 2.3214f - 3.14f * -4.7f + 5.1f + 6.75f - 7.32f * 8.61f + 9.19f);

            // Test 3
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test = 12.62 + 54.2354 / 2.1");
            data = this.GetGlobalVariableData("test", environment);
            this.AssertFloat(data, 12.62f + 54.2354f / 2.1f);

            // Test 3
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test = 5.5 / 2.25");
            data = this.GetGlobalVariableData("test", environment);
            this.AssertFloat(data, 5.5f / 2.25f);

            // Test 3
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test = 4.1 / 3.2 + 12.3 * 1.4 / 3.5");
            data = this.GetGlobalVariableData("test", environment);
            this.AssertFloat(data, 4.1f / 3.2f + 12.3f * 1.4f / 3.5f);

            // Test 4
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test = 234.64 + (12.243 - -123.542 * 2.45) / (123.671 * (1.1 + 2.2 + 3.3 / 12.12))");
            data = this.GetGlobalVariableData("test", environment);
            this.AssertFloat(data, 234.64f + (12.243f - -123.542f * 2.45f) / (123.671f * (1.1f + 2.2f + 3.3f / 12.12f)));

            // Test 5
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test = 23.42 % 3.234 + 12.123 % -8.43");
            data = this.GetGlobalVariableData("test", environment);
            this.AssertFloat(data, 23.42f % 3.234f + 12.123f % -8.43f);

            // Test 6
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test = 234.3 + (12.12 - 123.65 * 2.64) % (123.543 * (1.76 + 2.31 + 3.342 / 12.435)) % 12.543 + 54.453 / 2.76");
            data = this.GetGlobalVariableData("test", environment);
            this.AssertFloat(data, 234.3f + (12.12f - 123.65f * 2.64f) % (123.543f * (1.76f + 2.31f + 3.342f / 12.435f)) % 12.543f + 54.453f / 2.76f);
        }

        /// <summary>
        /// Tests arithmetic between integers and floats.
        /// </summary>
        [TestMethod]
        public void IntegerAndFloatArithmetic()
        {
            // Test 1
            var environment = this.ExecuteProgramWithoutErrors("var test = 1.41 + 2 + 3.12 + 4.76 + 5.123 + 6 + 7.72 + -8.12 + 9");
            var data = this.GetGlobalVariableData("test", environment);
            this.AssertFloat(data, 1.41f + 2 + 3.12f + 4.76f + 5.123f + 6 + 7.72f + -8.12f + 9);

            // Test 2
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test = 1.62 + 2.3214 - 3.14 * -4 + 5.1 + 6.75 - 7 * 8.61 + 9.19");
            data = this.GetGlobalVariableData("test", environment);
            this.AssertFloat(data, 1.62f + 2.3214f - 3.14f * -4 + 5.1f + 6.75f - 7 * 8.61f + 9.19f);

            // Test 3
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test = 12.62 + 54.2354 / 2");
            data = this.GetGlobalVariableData("test", environment);
            this.AssertFloat(data, 12.62f + 54.2354f / 2);

            // Test 3
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test = 5 / 2.25");
            data = this.GetGlobalVariableData("test", environment);
            this.AssertFloat(data, 5 / 2.25f);

            // Test 3
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test = 4.1 / 3.2 + 12 * 1.4 / 3.5");
            data = this.GetGlobalVariableData("test", environment);
            this.AssertFloat(data, 4.1f / 3.2f + 12 * 1.4f / 3.5f);

            // Test 4
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test = 234.64 + (12.243 - -123.542 * 2) / (123 * (1.1 + 2.2 + 3.3 / 12))");
            data = this.GetGlobalVariableData("test", environment);
            this.AssertFloat(data, 234.64f + (12.243f - -123.542f * 2) / (123 * (1.1f + 2.2f + 3.3f / 12)));

            // Test 5
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test = 23 % 3.234 + 12 % -8.43");
            data = this.GetGlobalVariableData("test", environment);
            this.AssertFloat(data, 23 % 3.234f + 12 % -8.43f);

            // Test 6
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test = 234 + (12.12 - 123.65 * 2.64) % (123 * (1.76 + 2 + 3.342 / 12.435)) % 12 + 54.453 / 2.76");
            data = this.GetGlobalVariableData("test", environment);
            this.AssertFloat(data, 234 + (12.12f - 123.65f * 2.64f) % (123 * (1.76f + 2 + 3.342f / 12.435f)) % 12 + 54.453f / 2.76f);
        }

        /// <summary>
        /// Tests the conditional And.
        /// </summary>
        [TestMethod]
        public void ConditionalAnd()
        {
            // Test Conditional And 1
            var environment = this.ExecuteProgramWithoutErrors("var test1 = 0",
                                                  "function Thing(b) { test1 = 1; return b; }",
                                                  "var test2 = true && Thing(true)");
            var data = this.GetGlobalVariableData("test1", environment);
            this.AssertInteger(data, 1);
            data = this.GetGlobalVariableData("test2", environment);
            this.AssertBoolean(data, true);

            // Test Conditional And 2
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 0",
                                              "function Thing(b) { test1 = 1; return b; }",
                                              "var test2 = false && Thing(true)");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertInteger(data, 0);
            data = this.GetGlobalVariableData("test2", environment);
            this.AssertBoolean(data, false);

            // Test Conditional And 3
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 0",
                                              "function Thing(b) { test1 = 1; return b; }",
                                              "var test2 = true && true && Thing(false)");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertInteger(data, 1);
            data = this.GetGlobalVariableData("test2", environment);
            this.AssertBoolean(data, false);

            // Test Conditional And 4
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 0",
                                              "function Thing(b) { test1 = 1; return b; }",
                                              "var test2 = true && false && Thing(true)");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertInteger(data, 0);
            data = this.GetGlobalVariableData("test2", environment);
            this.AssertBoolean(data, false);

            // Test errors
            this.Reset();
            environment = this.ExecuteProgram("var test1 = true && 1");
            Assert.AreEqual(ErrorCode.NonBooleanDataType, mErrorLog.Entries[0].ErrorCode);
            this.Reset();
            environment = this.ExecuteProgram("var test1 = true && 1.34");
            Assert.AreEqual(ErrorCode.NonBooleanDataType, mErrorLog.Entries[0].ErrorCode);
            this.Reset();
            environment = this.ExecuteProgram("var test1 = true && \"Help\"");
            Assert.AreEqual(ErrorCode.NonBooleanDataType, mErrorLog.Entries[0].ErrorCode);
            this.Reset();
            environment = this.ExecuteProgram("define Thing {}",
                                              "var test1 = true && new Thing()");
            Assert.AreEqual(ErrorCode.NonBooleanDataType, mErrorLog.Entries[0].ErrorCode);
        }

        /// <summary>
        /// Tests the conditional Or.
        /// </summary>
        [TestMethod]
        public void ConditionalOr()
        {
            // Test Conditional Or 1
            var environment = this.ExecuteProgramWithoutErrors("var test1 = 0",
                                                  "function Thing(b) { test1 = 1; return b; }",
                                                  "var test2 = true || Thing(true)");
            var data = this.GetGlobalVariableData("test1", environment);
            this.AssertInteger(data, 0);
            data = this.GetGlobalVariableData("test2", environment);
            this.AssertBoolean(data, true);

            // Test Conditional Or 2
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 0",
                                              "function Thing(b) { test1 = 1; return b; }",
                                              "var test2 = false || Thing(true)");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertInteger(data, 1);
            data = this.GetGlobalVariableData("test2", environment);
            this.AssertBoolean(data, true);

            // Test Conditional Or 3
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 0",
                                              "function Thing(b) { test1 = 1; return b; }",
                                              "var test2 = false || Thing(false)");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertInteger(data, 1);
            data = this.GetGlobalVariableData("test2", environment);
            this.AssertBoolean(data, false);

            // Test Conditional Or 4
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 0",
                                              "function Thing(b) { test1 = 1; return b; }",
                                              "var test2 = false || false || Thing(true)");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertInteger(data, 1);
            data = this.GetGlobalVariableData("test2", environment);
            this.AssertBoolean(data, true);

            // Test Conditional Or 5
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 0",
                                              "function Thing(b) { test1 += 1; return b; }",
                                              "var test2 = Thing(false) || Thing(false) || Thing(true)");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertInteger(data, 3);
            data = this.GetGlobalVariableData("test2", environment);
            this.AssertBoolean(data, true);

            // Test Conditional Or 6
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 0",
                                              "function Thing(b) { test1 += 1; return b; }",
                                              "var test2 = Thing(true) || Thing(true) || Thing(true)");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertInteger(data, 1);
            data = this.GetGlobalVariableData("test2", environment);
            this.AssertBoolean(data, true);
            
            // Test errors
            this.Reset();
            environment = this.ExecuteProgram("var test1 = 1 || true");
            Assert.AreEqual(ErrorCode.NonBooleanDataType, mErrorLog.Entries[0].ErrorCode);
            this.Reset();
            environment = this.ExecuteProgram("var test1 = 1.34 || true");
            Assert.AreEqual(ErrorCode.NonBooleanDataType, mErrorLog.Entries[0].ErrorCode);
            this.Reset();
            environment = this.ExecuteProgram("var test1 = \"Help\" || true");
            Assert.AreEqual(ErrorCode.NonBooleanDataType, mErrorLog.Entries[0].ErrorCode);
            this.Reset();
            environment = this.ExecuteProgram("define Thing {}",
                                              "var test1 = new Thing() || true");
            Assert.AreEqual(ErrorCode.NonBooleanDataType, mErrorLog.Entries[0].ErrorCode);
        }

        /// <summary>
        /// Tests the logical boolean And.
        /// </summary>
        [TestMethod]
        public void LogicalAnd()
        {
            // Test Logical And 1
            var environment = this.ExecuteProgramWithoutErrors("var test1 = 0",
                                                  "function Thing(b) { test1 = 1; return b; }",
                                                  "var test2 = true & Thing(true)");
            var data = this.GetGlobalVariableData("test1", environment);
            this.AssertInteger(data, 1);
            data = this.GetGlobalVariableData("test2", environment);
            this.AssertBoolean(data, true);

            // Test Logical And 2
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 0",
                                              "function Thing(b) { test1 = 1; return b; }",
                                              "var test2 = false & Thing(true)");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertInteger(data, 1);
            data = this.GetGlobalVariableData("test2", environment);
            this.AssertBoolean(data, false);

            // Test Logical And 3
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 0",
                                              "function Thing(b) { test1 += 1; return b; }",
                                              "var test2 = Thing(false) & Thing(false) & Thing(false)");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertInteger(data, 3);
            data = this.GetGlobalVariableData("test2", environment);
            this.AssertBoolean(data, false);

            // Test errors
            this.Reset();
            environment = this.ExecuteProgram("var test1 = true & 1");
            Assert.AreEqual(ErrorCode.NonBooleanDataType, mErrorLog.Entries[0].ErrorCode);
            this.Reset();
            environment = this.ExecuteProgram("var test1 = true & 1.34");
            Assert.AreEqual(ErrorCode.NonBooleanDataType, mErrorLog.Entries[0].ErrorCode);
            this.Reset();
            environment = this.ExecuteProgram("var test1 = true & \"Help\"");
            Assert.AreEqual(ErrorCode.NonBooleanDataType, mErrorLog.Entries[0].ErrorCode);
            this.Reset();
            environment = this.ExecuteProgram("define Thing {}",
                                              "var test1 = true & new Thing()");
            Assert.AreEqual(ErrorCode.NonBooleanDataType, mErrorLog.Entries[0].ErrorCode);
        }

        /// <summary>
        /// Tests the logical boolean Or.
        /// </summary>
        [TestMethod]
        public void LogicalOr()
        {
            // Test Logical Or 1
            var environment = this.ExecuteProgramWithoutErrors("var test1 = 0",
                                                  "function Thing(b) { test1 = 1; return b; }",
                                                  "var test2 = true | Thing(true)");
            var data = this.GetGlobalVariableData("test1", environment);
            this.AssertInteger(data, 1);
            data = this.GetGlobalVariableData("test2", environment);
            this.AssertBoolean(data, true);

            // Test Logical Or 2
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 0",
                                              "function Thing(b) { test1 = 1; return b; }",
                                              "var test2 = false | Thing(true)");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertInteger(data, 1);
            data = this.GetGlobalVariableData("test2", environment);
            this.AssertBoolean(data, true);

            // Test Logical Or 3
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 0",
                                              "function Thing(b) { test1 += 1; return b; }",
                                              "var test2 = Thing(false) | Thing(false) | Thing(true)");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertInteger(data, 3);
            data = this.GetGlobalVariableData("test2", environment);
            this.AssertBoolean(data, true);

            // Test Logical Or 4
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 0",
                                              "function Thing(b) { test1 += 1; return b; }",
                                              "var test2 = Thing(true) | Thing(true) | Thing(true)");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertInteger(data, 3);
            data = this.GetGlobalVariableData("test2", environment);
            this.AssertBoolean(data, true);

            // Test errors
            this.Reset();
            environment = this.ExecuteProgram("var test1 = true | 1");
            Assert.AreEqual(ErrorCode.NonBooleanDataType, mErrorLog.Entries[0].ErrorCode);
            this.Reset();
            environment = this.ExecuteProgram("var test1 = true | 1.34");
            Assert.AreEqual(ErrorCode.NonBooleanDataType, mErrorLog.Entries[0].ErrorCode);
            this.Reset();
            environment = this.ExecuteProgram("var test1 = true | \"Help\"");
            Assert.AreEqual(ErrorCode.NonBooleanDataType, mErrorLog.Entries[0].ErrorCode);
            this.Reset();
            environment = this.ExecuteProgram("define Thing {}",
                                              "var test1 = true | new Thing()");
            Assert.AreEqual(ErrorCode.NonBooleanDataType, mErrorLog.Entries[0].ErrorCode);
        }

        /// <summary>
        /// Tests boolean arithmetic.
        /// </summary>
        [TestMethod]
        public void BooleanArithmetic()
        {
            // Test 1
            var environment = this.ExecuteProgramWithoutErrors("var test1 = 0",
                                                  "function Bool(b) { test1 += 1; return b; }",
                                                  "var test2 = Bool(true) && Bool(true) || Bool(false)");
            var data = this.GetGlobalVariableData("test1", environment);
            this.AssertInteger(data, 2);
            data = this.GetGlobalVariableData("test2", environment);
            this.AssertBoolean(data, true);

            // Test 2
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 0",
                                              "function Bool(b) { test1 += 1; return b; }",
                                              "var test2 = Bool(true) && Bool(false) || Bool(true)");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertInteger(data, 3);
            data = this.GetGlobalVariableData("test2", environment);
            this.AssertBoolean(data, true);

            // Test 3
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 0",
                                              "function Bool(b) { test1 += 1; return b; }",
                                              "var test2 = Bool(true) || Bool(true) && Bool(false)");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertInteger(data, 1);
            data = this.GetGlobalVariableData("test2", environment);
            this.AssertBoolean(data, true || true && false);

            // Test 4
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 0",
                                              "function Bool(b) { test1 += 1; return b; }",
                                              "var test2 = Bool(true) | Bool(false) && Bool(false)");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertInteger(data, 3);
            data = this.GetGlobalVariableData("test2", environment);
            this.AssertBoolean(data, false);

            // Test 5
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 0",
                                              "function Bool(b) { test1 += 1; return b; }",
                                              "var test2 = Bool(false) || Bool(true) & Bool(true)");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertInteger(data, 3);
            data = this.GetGlobalVariableData("test2", environment);
            this.AssertBoolean(data, true);

            // Test 6
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 0",
                                              "function Bool(b) { test1 += 1; return b; }",
                                              "var test2 = Bool(false) & Bool(true) || Bool(true)");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertInteger(data, 3);
            data = this.GetGlobalVariableData("test2", environment);
            this.AssertBoolean(data, true);

            // Test 7
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 0",
                                              "function Bool(b) { test1 += 1; return b; }",
                                              "var test2 = Bool(true) & Bool(true) || Bool(false)");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertInteger(data, 2);
            data = this.GetGlobalVariableData("test2", environment);
            this.AssertBoolean(data, true);
        }

        /// <summary>
        /// Tests the equality of data.
        /// </summary>
        [TestMethod]
        public void IntegerEquality()
        {
            #region Equality

            // Test 1
            var environment = this.ExecuteProgramWithoutErrors("var test1 = 1 == 1");
            var data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, true);

            // Test 2
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 34314 == 34314");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, true);

            // Test 3
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 12 == 6");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, false);

            // Test 4
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 2327 == 65878");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, false);

            #endregion

            #region Greater Than

            // Test 5
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 1 > 6");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, false);

            // Test 6
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 7 > 6");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, true);

            // Test 7
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 6 > 6");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, false);

            #endregion

            #region Less Than

            // Test 8
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 1 < 6");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, true);

            // Test 9
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 7 < 6");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, false);

            // Test 10
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 6 < 6");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, false);

            #endregion

            #region Less Than or Equal To

            // Test 11
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 1 <= 6");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, true);

            // Test 12
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 7 <= 6");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, false);

            // Test 13
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 6 <= 6");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, true);

            #endregion

            #region Greater Than or Equal To

            // Test 14
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 1 >= 6");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, false);

            // Test 15
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 7 >= 6");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, true);

            // Test 16
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 6 >= 6");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, true);

            #endregion

            #region Not Equal

            // Test 17
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 1 != 6");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, true);

            // Test 18
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 7 != 6");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, true);

            // Test 19
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 6 != 6");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, false);

            #endregion

            #region Not Equal (Other Types)

            // Test 20
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 1 != 12.123");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, true);

            // Test 21
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 1 != 1.0");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, false);

            // Test 22
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 1 != \"Hello World\"");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, true);

            // Test 23
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 1 != true");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, true);

            // Test 24
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("define Thing {}",
                                              "var test1 = 1 != new Thing()");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, true);

            #endregion

            #region Equal To (Other Types)

            // Test 25
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 1 == 12.123");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, false);

            // Test 26
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 1 == 1.0");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, true);

            // Test 27
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 1 == \"Hello World\"");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, false);

            // Test 28
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 1 == true");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, false);

            // Test 29
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("define Thing {}",
                                              "var test1 = 1 == new Thing()");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, false);

            #endregion
        }

        /// <summary>
        /// Tests the equality of data.
        /// </summary>
        [TestMethod]
        public void FloatEquality()
        {
            #region Equality

            // Test 1
            var environment = this.ExecuteProgramWithoutErrors("var test1 = 1.1234 == 1.1234");
            var data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, true);

            // Test 2
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 34314.234 == 34314.234");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, true);

            // Test 3
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 234.234 == -454.122");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, false);

            // Test 4
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = -2327 == -65878");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, false);

            #endregion

            #region Greater Than

            // Test 5
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 34.3432 > 545.2212");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, false);

            // Test 6
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 334.131 > 12.0");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, true);

            // Test 7
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 6.121 > 6.121");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, false);

            #endregion

            #region Less Than

            // Test 8
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 1.12334 < 6.2313");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, true);

            // Test 9
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 7.2344 < 6.756");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, false);

            // Test 10
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 6.543 < 6.543");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, false);

            #endregion

            #region Less Than or Equal To

            // Test 11
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 1.3424 <= 6.6743");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, true);

            // Test 12
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 7.423 <= 6.6464");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, false);

            // Test 13
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 6.642 <= 6.642");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, true);

            #endregion

            #region Greater Than or Equal To

            // Test 14
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 1.34234 >= 6.652");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, false);

            // Test 15
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 7.323 >= 6.664");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, true);

            // Test 16
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 6.246 >= 6.246");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, true);

            #endregion

            #region Not Equal

            // Test 17
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 1.2134 != 6.633");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, true);

            // Test 18
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 7.752 != 6.236");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, true);

            // Test 19
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 6.441 != 6.441");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, false);

            #endregion

            #region Not Equal (Other Types)

            // Test 20
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 12.123 != 1");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, true);

            // Test 21
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 1.0 != 1");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, false);

            // Test 22
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 546.2314 != \"Hello World\"");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, true);

            // Test 23
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 1.623 != true");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, true);

            // Test 24
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("define Thing {}",
                                              "var test1 = 7.21 != new Thing()");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, true);

            #endregion

            #region Equal To (Other Types)

            // Test 25
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 12.123 == 1");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, false);

            // Test 26
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 234.0 == 234");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, true);

            // Test 27
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 2135.123 == \"Hello World\"");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, false);

            // Test 28
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = 82.47 == true");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, false);

            // Test 29
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("define Thing {}",
                                              "var test1 = 37231.437 == new Thing()");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, false);

            #endregion
        }

        /// <summary>
        /// Tests the equality of data.
        /// </summary>
        [TestMethod]
        public void StringEquality()
        {
            #region Equality

            // Test 1
            var environment = this.ExecuteProgramWithoutErrors("var test1 = \"Help\" == \"Help\"");
            var data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, true);

            // Test 2
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = \"Hello World, how're you?\" == \"Hello World, how're you?\"");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, true);

            // Test 3
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = \"Help\" == \"Hello World\"");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, false);

            #endregion

            #region Greater Than

            // Test 5
            this.Reset();
            environment = this.ExecuteProgram("var test1 = \"Help\" > 545.2212");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            #endregion

            #region Less Than

            // Test 8
            this.Reset();
            environment = this.ExecuteProgram("var test1 = \"Help\" < 6.2313");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            #endregion

            #region Less Than or Equal To

            // Test 11
            this.Reset();
            environment = this.ExecuteProgram("var test1 = \"Help\" <= 6.6743");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            #endregion

            #region Greater Than or Equal To

            // Test 14
            this.Reset();
            environment = this.ExecuteProgram("var test1 = \"Help\" >= 6.652");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            #endregion

            #region Not Equal

            // Test 17
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = \"Help\" != \"Help Me Out Here!\"");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, true);

            // Test 18
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = \"Apples\" != \"Bananas\"");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, true);

            // Test 19
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = \"Help\" != \"Help\"");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, false);

            #endregion

            #region Not Equal (Other Types)

            // Test 20
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = \"Help\" != 1");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, true);

            // Test 22
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = \"Help\" != 12.5423");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, true);

            // Test 23
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = \"Help\" != true");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, true);

            // Test 24
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("define Thing {}",
                                              "var test1 = \"Help\" != new Thing()");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, true);

            #endregion

            #region Equal To (Other Types)

            // Test 25
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = \"Help\" == 1");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, false);

            // Test 26
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = \"Help\" == 234.3463");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, false);

            // Test 28
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = \"Help\" == true");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, false);

            // Test 29
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("define Thing {}",
                                              "var test1 = \"Help\" == new Thing()");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, false);

            #endregion
        }

        /// <summary>
        /// Tests the equality of data.
        /// </summary>
        [TestMethod]
        public void BooleanEquality()
        {
            #region Equality

            // Test 1
            var environment = this.ExecuteProgramWithoutErrors("var test1 = true == true");
            var data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, true);

            // Test 2
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = false == false");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, true);

            // Test 3
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = true == false");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, false);

            #endregion

            #region Greater Than

            // Test 5
            this.Reset();
            environment = this.ExecuteProgram("var test1 = true > 545.2212");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            #endregion

            #region Less Than

            // Test 8
            this.Reset();
            environment = this.ExecuteProgram("var test1 = false < 6.2313");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            #endregion

            #region Less Than or Equal To

            // Test 11
            this.Reset();
            environment = this.ExecuteProgram("var test1 = true <= 6.6743");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            #endregion

            #region Greater Than or Equal To

            // Test 14
            this.Reset();
            environment = this.ExecuteProgram("var test1 = false >= 6.652");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            #endregion

            #region Not Equal

            // Test 17
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = true != false");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, true);

            // Test 18
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = false != true");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, true);

            // Test 19
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = true != true");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, false);

            #endregion

            #region Not Equal (Other Types)

            // Test 20
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = false != 1");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, true);

            // Test 22
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = true != 12.5423");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, true);

            // Test 23
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = false != \"false\"");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, true);

            // Test 24
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("define Thing {}",
                                              "var test1 = true != new Thing()");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, true);

            #endregion

            #region Equal To (Other Types)

            // Test 25
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = true == 1");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, false);

            // Test 26
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = false == 234.3463");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, false);

            // Test 28
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test1 = true == \"true\"");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, false);

            // Test 29
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("define Thing {}",
                                              "var test1 = false == new Thing()");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, false);

            #endregion
        }


        /// <summary>
        /// Tests the equality of data.
        /// </summary>
        [TestMethod]
        public void ObjectEquality()
        {
            #region Equality

            // Test 1
            var environment = this.ExecuteProgramWithoutErrors("define A { var a; var b; constructor(c, d) { a = c; b = d}}",
                                                  "define B { var a; constructor(b) { a = b; }}",
                                                  "var a1 = new A(1, 1)",
                                                  "var a2 = a1",
                                                  "var test1 = a1 == a2");
            var data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, true);
            
            // Test 2
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("define A { var a; var b; constructor(c, d) { a = c; b = d}}",
                                              "define B { var a; constructor(b) { a = b; }}",
                                              "var a1 = new A(1, 1)",
                                              "var a2 = new A(1, 2)",
                                              "var test1 = a1 == a2");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, false);

            // Test 3
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("define A { var a; var b; constructor(c, d) { a = c; b = d}}",
                                              "define B { var a; constructor(b) { a = b; }}",
                                              "var a1 = new A(1, 1)",
                                              "var a2 = new B(1)",
                                              "var test1 = a1 == a2");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, false);


            // Test recursion
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("define A { var a; var b; constructor(c, d) { a = c; b = d}}",
                                              "var a1 = new A(1, 1)",
                                              "var a2 = new A(1, 1)",
                                              "a1.a = a1",
                                              "a2.a = a2",
                                              "var test1 = a1 == a2");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, false);

            #endregion

            #region Greater Than

            // Test 5
            this.Reset();
            environment = this.ExecuteProgram("define A { var a; var b; constructor(c, d) { a = c; b = d}}",
                                              "var a1 = new A(1, 1)",
                                              "var test1 = a1 > 6673.34523");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            #endregion

            #region Less Than

            // Test 8
            this.Reset();
            environment = this.ExecuteProgram("define A { var a; var b; constructor(c, d) { a = c; b = d}}",
                                              "var a1 = new A(1, 1)",
                                              "var test1 = a1 < 6.2313");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            #endregion

            #region Less Than or Equal To

            // Test 11
            this.Reset();
            environment = this.ExecuteProgram("define A { var a; var b; constructor(c, d) { a = c; b = d}}",
                                              "var a1 = new A(1, 1)",
                                              "var test1 = a1 <= 6.6743");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            #endregion

            #region Greater Than or Equal To

            // Test 14
            this.Reset();
            environment = this.ExecuteProgram("define A { var a; var b; constructor(c, d) { a = c; b = d}}",
                                              "var a1 = new A(1, 1)",
                                              "var test1 = a1 >= 6.652");
            Assert.AreEqual(ErrorCode.TypeMismatch, mErrorLog.Entries[0].ErrorCode);

            #endregion

            #region Not Equal

            // Test 17
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("define A { var a; var b; constructor(c, d) { a = c; b = d}}",
                                              "define B { var a; constructor(b) { a = b; }}",
                                              "var a1 = new A(1, 1)",
                                              "var a2 = new B(1)",
                                              "var test1 = a1 != a2");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, true);

            // Test 18
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("define A { var a; var b; constructor(c, d) { a = c; b = d}}",
                                              "define B { var a; constructor(b) { a = b; }}",
                                              "var a1 = new A(\"Help\", 643)",
                                              "var a2 = new A(1, 12)",
                                              "var test1 = a1 != a2");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, true);

            // Test 19
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("define A { var a; var b; constructor(c, d) { a = c; b = d}}",
                                              "define B { var a; constructor(b) { a = b; }}",
                                              "var a1 = new A(1, 1)",
                                              "var a2 = a1",
                                              "var test1 = a1 != a2");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, false);

            #endregion

            #region Not Equal (Other Types)

            // Test 20
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("define A { var a; var b; constructor(c, d) { a = c; b = d}}",
                                              "var a1 = new A(1, 1)",
                                              "var test1 = a1 != 1");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, true);

            // Test 22
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("define A { var a; var b; constructor(c, d) { a = c; b = d}}",
                                              "var a1 = new A(1, 1)",
                                              "var test1 = a1 != 12.5423");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, true);

            // Test 23
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("define A { var a; var b; constructor(c, d) { a = c; b = d}}",
                                              "var a1 = new A(1, 1)",
                                              "var test1 = a1 != \"false\"");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, true);

            // Test 24
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("define A { var a; var b; constructor(c, d) { a = c; b = d}}",
                                              "var a1 = new A(1, 1)",
                                              "var test1 = a1 != true");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, true);

            #endregion

            #region Equal To (Other Types)

            // Test 25
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("define A { var a; var b; constructor(c, d) { a = c; b = d}}",
                                              "var a1 = new A(1, 1)",
                                              "var test1 = a1 == 1");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, false);

            // Test 26
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("define A { var a; var b; constructor(c, d) { a = c; b = d}}",
                                              "var a1 = new A(1, 1)",
                                              "var test1 = a1 == 234.3463");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, false);

            // Test 28
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("define A { var a; var b; constructor(c, d) { a = c; b = d}}",
                                              "var a1 = new A(1, 1)",
                                              "var test1 = a1 == \"true\"");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, false);

            // Test 29
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("define A { var a; var b; constructor(c, d) { a = c; b = d}}",
                                              "var a1 = new A(1, 1)",
                                              "var test1 = a1 == false");
            data = this.GetGlobalVariableData("test1", environment);
            this.AssertBoolean(data, false);

            #endregion
        }

        /// <summary>
        /// Tests that expressions are executed in the correct order.
        /// </summary>
        [TestMethod]
        public void ExpressionExecutionOrder()
        {
            var environment = this.ExecuteProgramWithoutErrors("var test1 = 1 + 1 + \"1\"");
            var data = this.GetGlobalVariableData("test1", environment);
            this.AssertString(data, "21");
        }

        /// <summary>
        /// Tests that type constructors work.
        /// </summary>
        [TestMethod]
        public void TypeConstructors()
        {
            // Test Integer
            var environment = this.ExecuteProgramWithoutErrors("var test = new Integer()");
            var data = this.GetGlobalVariableData("test", environment);
            this.AssertInteger(data, 0);

            // Test Float
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test = new Float()");
            data = this.GetGlobalVariableData("test", environment);
            this.AssertFloat(data, 0.0f);

            // Test String
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test = new String()");
            data = this.GetGlobalVariableData("test", environment);
            this.AssertString(data, string.Empty);

            // Test Boolean
            this.Reset();
            environment = this.ExecuteProgramWithoutErrors("var test = new Boolean()");
            data = this.GetGlobalVariableData("test", environment);
            this.AssertBoolean(data, false);
        }

        /// <summary>
        /// Tests that the number of parameters passed to functions is checked correctly.
        /// </summary>
        [TestMethod]
        public void ParameterCounts()
        {
            // Test Correct
            this.ExecuteProgram("function Thing(a) {}",
                                "Thing(6)");
            Assert.AreEqual(0, mErrorLog.Entries.Count);

            // Test too many
            this.Reset();
            this.ExecuteProgram("function Thing(a) {}",
                                "Thing(6, \"Help\")");
            Assert.AreEqual(ErrorCode.InsufficientParameters, mErrorLog.Entries[0].ErrorCode);

            // Test too few
            this.Reset();
            this.ExecuteProgram("function Thing(a, b, c) {}",
                                "Thing(\"Help\")");
            Assert.AreEqual(ErrorCode.InsufficientParameters, mErrorLog.Entries[0].ErrorCode);
        }

        /// <summary>
        /// Tests that data can be printed to the console without any errors.
        /// </summary>
        [TestMethod]
        public void PrintToConsole()
        {
            // Test String
            this.ExecuteProgramWithoutErrors("var a = \"I Am A String\"",
                                "Console.Print(a);Console.PrintLine(a)");
            Assert.AreEqual(0, mErrorLog.Entries.Count);

            // Test Integer
            this.Reset();
            this.ExecuteProgramWithoutErrors("var a = 5",
                                "Console.Print(a);Console.PrintLine(a)");
            Assert.AreEqual(0, mErrorLog.Entries.Count);

            // Test Float
            this.Reset();
            this.ExecuteProgramWithoutErrors("var a = 23.354123",
                                "Console.Print(a);Console.PrintLine(a)");
            Assert.AreEqual(0, mErrorLog.Entries.Count);

            // Test Boolean
            this.Reset();
            this.ExecuteProgramWithoutErrors("var a = true",
                                "Console.Print(a);Console.PrintLine(a)");
            Assert.AreEqual(0, mErrorLog.Entries.Count);

            // Test Object
            this.Reset();
            this.ExecuteProgramWithoutErrors("define Person {}", 
                                "var a = new Person()",
                                "Console.Print(a);Console.PrintLine(a)");
            Assert.AreEqual(0, mErrorLog.Entries.Count);

            // Test List
            this.Reset();
            this.ExecuteProgramWithoutErrors("var a = new List()",
                                "Console.Print(a);Console.PrintLine(a)");
            Assert.AreEqual(0, mErrorLog.Entries.Count);

            // Test Dictionary
            this.Reset();
            this.ExecuteProgramWithoutErrors("var a = new Dictionary()",
                                "Console.Print(a);Console.PrintLine(a)");
            Assert.AreEqual(0, mErrorLog.Entries.Count);
        }

        /// <summary>
        /// Tests that values are returned properly.
        /// </summary>
        [TestMethod]
        public void ReturnValues()
        {
            // Test standard usage
            this.ExecuteProgramWithoutErrors("var test = Thing(); function Thing() { return 5 }");
            var data = this.GetGlobalVariableData("test", mExecutionEnvironment);
            this.AssertInteger(data, 5);

            // Test within if statement
            this.Reset();
            this.ExecuteProgramWithoutErrors("var test = Thing(); function Thing() { if true { return 5 } }");
            data = this.GetGlobalVariableData("test", mExecutionEnvironment);
            this.AssertInteger(data, 5);

            // Test within while loop
            this.Reset();
            this.ExecuteProgramWithoutErrors("var test = Thing(); function Thing() { while true { return 5 } }");
            data = this.GetGlobalVariableData("test", mExecutionEnvironment);
            this.AssertInteger(data, 5);
        }

        /// <summary>
        /// Assert that the data is null.
        /// </summary>
        /// <param name="data">The data to check.</param>
        private void AssertNull(Data data)
        {
            Assert.AreEqual(DataType.Null, data.Type);
        }

        /// <summary>
        /// Assert that the data is a boolean.
        /// </summary>
        /// <param name="data">The data to check.</param>
        /// <param name="value">The value to compare.</param>
        private void AssertBoolean(Data data, bool value)
        {
            Assert.AreEqual(DataType.Boolean, data.Type);
            var floatData = (BooleanData)data;
            Assert.AreEqual(value, floatData.Value);
        }

        /// <summary>
        /// Assert that the data is a float.
        /// </summary>
        /// <param name="data">The data to check.</param>
        /// <param name="value">The value to compare.</param>
        private void AssertFloat(Data data, float value)
        {
            Assert.AreEqual(DataType.Float, data.Type);
            var floatData = (FloatData)data;
            Assert.AreEqual(value, floatData.Value, 0.0001f);
        }

        /// <summary>
        /// Assert that the data is a string.
        /// </summary>
        /// <param name="data">The data to check.</param>
        /// <param name="value">The value to compare.</param>
        private void AssertString(Data data, string value)
        {
            Assert.AreEqual(DataType.String, data.Type);
            var stringData = (StringData)data;
            Assert.AreEqual(value, stringData.Value);
        }

        /// <summary>
        /// Assert that the data is an integer.
        /// </summary>
        /// <param name="data">The data to check.</param>
        /// <param name="value">The value to compare.</param>
        private void AssertInteger(Data data, int value)
        {
            Assert.AreEqual(DataType.Integer, data.Type);
            var intData = (IntegerData)data;
            Assert.AreEqual(value, intData.Value);
        }
    }
}