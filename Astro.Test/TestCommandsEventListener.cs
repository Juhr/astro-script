﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Astro.Engine.Interpreter.ExecutionEnvironment;

namespace Astro.Test
{
    /// <summary>
    /// A commands event listener for testing purposes.
    /// </summary>
    public class TestCommandsEventListener : ICommandsEventListener
    {
        public event EventHandler<EventDetectedEventArgs> EventStartDetectedEvent;

        public event EventHandler<EventDetectedEventArgs> EventStopDetectedEvent;

        public event EventHandler<EventDetectedEventArgs> EventRepeatDetectedEvent;
    }
}
