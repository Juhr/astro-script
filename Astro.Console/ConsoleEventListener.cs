﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Astro.Engine.Interpreter.ExecutionEnvironment;

namespace Astro.Console
{
    /// <summary>
    /// The commands event listener for the console.
    /// </summary>
    public class ConsoleEventListener : ICommandsEventListener
    {
        /// <summary>
        /// Raised when this listener detects an event's start.
        /// </summary>
        public event EventHandler<EventDetectedEventArgs> EventStartDetectedEvent;

        /// <summary>
        /// Raised when this listener detects an event's end.
        /// </summary>
        public event EventHandler<EventDetectedEventArgs> EventStopDetectedEvent;

        /// <summary>
        /// Raised when this listener detects an event's repetition.
        /// </summary>
        public event EventHandler<EventDetectedEventArgs> EventRepeatDetectedEvent;

        /// <summary>
        /// Create the event listener.
        /// </summary>
        public ConsoleEventListener()
        { }

        /// <summary>
        /// Listen for console events.
        /// </summary>
        public void ListenForEvents()
        {
            while (true)
            {
                // Get a key press event
                var keyString = System.Console.ReadKey(true).KeyChar.ToString().ToUpper();

                // Raise the start event
                if (this.EventStartDetectedEvent != null)
                {
                    this.EventStartDetectedEvent(this, new EventDetectedEventArgs(keyString));
                }
            }
        }
    }
}
