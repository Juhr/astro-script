﻿using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using Astro.Engine.ErrorReporting;
using Astro.Engine.Interpreter;
using Astro.Engine.Interpreter.ExecutionEnvironment;
using Astro.Engine.Parsing;
using Astro.Engine.Symbols;
using Astro.Engine.Tokenization;

namespace Astro.Console
{
    class Program
    {
        public static void Main(string[] args)
        {
            var sw = new Stopwatch();
            // Create stuff
            sw.Start();
            var parseNodeFactory = new InterpreterParseNodeFactory();
            var errorLog = new ErrorLog();
            var symbolTable = new SymbolTable(parseNodeFactory);
            var tokenizer = new Tokenizer(symbolTable, errorLog);
            var parser = new Parser(symbolTable, parseNodeFactory, errorLog);
            var eventListener = new ConsoleEventListener();
            var executionEnvironment = new InterpreterExecutionEnvironment(errorLog, eventListener, new ConsoleOutput());
            sw.Stop();
            System.Console.WriteLine("System Initialized: {0}ms", sw.ElapsedMilliseconds);

            // Read the source
            sw.Restart();
            var source = File.ReadAllText("TestCode.txt");
            sw.Stop();
            System.Console.WriteLine("Source File Read: {0}ms", sw.ElapsedMilliseconds);

            // Tokenize the source
            sw.Restart();
            TokenStream stream;
            tokenizer.Tokenize(source, out stream);
            sw.Stop();
            System.Console.WriteLine("Source Tokenized: {0}ms", sw.ElapsedMilliseconds);

            // Parse the program
            sw.Restart();
            ParseNode program;
            var parseResult = parser.Parse(stream, out program);
            sw.Stop();
            System.Console.WriteLine("Tokens Parsed: {0}ms", sw.ElapsedMilliseconds);

            // Only run or listen if parse was successful
            if (parseResult)
            {
                // Execute the program
                sw.Restart();
                executionEnvironment.ExecuteProgram((InterpreterParseNode)program);
                sw.Stop();
                System.Console.WriteLine("Program Executed: {0}ms", sw.ElapsedMilliseconds);

                // Listen for events
                //System.Console.WriteLine("");
                //System.Console.WriteLine("Listening For Events.");
                //eventListener.ListenForEvents();
            }

            // Print any error messages
            System.Console.ForegroundColor = System.ConsoleColor.Red;
            var errors = errorLog.Entries;
            for (int i = 0, length = errors.Count; i < length; i++)
            {
                System.Console.WriteLine("{0}. {1}", i, errors[i].Message);
            }
            System.Console.Read();
        }
    }
}
